#!/bin/bash

curl -X POST -H "Content-Type: application/json" \
             -d '{"firstName": "Michael", "lastName": "Gikaru", "email": "mgik@gmail.com", "nickname": "mgik", "preferredLanguage": "english", "country": "kenya", "city": "nairbori", "birthdate": "1995-04-23"  }' \
             localhost:8080/user

curl -X POST -H "Content-Type: application/json" \
             -d '{"firstName": "Sebastian", "lastName": "Moreau", "email": "sebastian@gmail.com", "nickname": "sebashack", "preferredLanguage": "english", "country": "macedonia", "city": "struga", "birthdate": "2003-04-23"  }' \
             localhost:8080/user/musician

curl -X POST -H "Content-Type: application/json" \
             -d '{"firstName": "Michael", "lastName": "Heart", "email": "mhear@gmail.com", "nickname": "myheart", "preferredLanguage": "english", "country": "scotland", "city": "edinburgh", "birthdate": "2000-05-03"  }' \
             localhost:8080/user/podcaster
