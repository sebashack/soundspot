#!/bin/bash

curl -X POST -H "Content-Type: application/json" \
             -d '{"name": "Acid Vibes", "description": "Acid Jazz and Cyber Punk", "coverImageLink": "https://some.cdn.com", "releaseDate": "2010-01-03"}' \
             localhost:8080/user/musician/873ad3e0-8836-43a3-83a2-9cd1ec6fe151/album

curl -X POST -H "Content-Type: application/json" \
             -d '{"name": "Productivity Flow", "description": "A show for hyperproductive people", "coverImageLink": "https://some.cdn.com", "releaseDate": "2015-03-04"}' \
             localhost:8080/user/podcaster/1bc62a06-8d34-4df1-8dcc-ce0152953dbd/show
