#!/bin/bash

curl -X POST -H "Content-Type: application/json" \
              -d '{"firstName": "Cerrone", "lastName": "Summer", "email": "cesumm@gmail.com", "nickname": "cesumm", "preferredLanguage": "english", "country": "dominica", "city": "rosalie", "birthdate": "1999-05-23"  }' \
              localhost:8080/user/ft-artist/song/33685fc0-92b0-4065-9f3c-b3776ed17fc6

curl -X POST -H "Content-Type: application/json" \
              -d '{"firstName": "Geneva", "lastName": "Jacuzzi", "email": "jacuzzi@gmail.com", "nickname": "jacuzzi", "preferredLanguage": "italian", "country": "italy", "city": "rome", "birthdate": "1982-04-13"  }' \
              localhost:8080/user/ft-artist/song/33685fc0-92b0-4065-9f3c-b3776ed17fc6

curl -X POST -H "Content-Type: application/json" \
             -d '{"firstName": "Michael", "lastName": "Klein", "email": "mklein@gmail.com", "nickname": "mklein", "preferredLanguage": "english", "country": "unites states of america", "city": "new york", "birthdate": "1997-08-23"  }' \
             localhost:8080/user/participant/episode/49298b90-6f17-4d4f-b987-46bab1b7c10e
