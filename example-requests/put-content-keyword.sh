#!/bin/bash

curl -X PUT localhost:8080/content/113631ff-9645-40ac-bf3d-1d970b483350/keyword?word='acid-jazz'
curl -X PUT localhost:8080/content/113631ff-9645-40ac-bf3d-1d970b483350/keyword?word='fusion'
curl -X PUT localhost:8080/content/33685fc0-92b0-4065-9f3c-b3776ed17fc6/keyword?word='indie'
curl -X PUT localhost:8080/content/33685fc0-92b0-4065-9f3c-b3776ed17fc6/keyword?word='experimental'
curl -X PUT localhost:8080/content/33685fc0-92b0-4065-9f3c-b3776ed17fc6/keyword?word='fusion'

curl -X PUT localhost:8080/content/49298b90-6f17-4d4f-b987-46bab1b7c10e/keyword?word='spirituality'
curl -X PUT localhost:8080/content/49298b90-6f17-4d4f-b987-46bab1b7c10e/keyword?word='mental-strength'
curl -X PUT localhost:8080/content/c37bad98-b622-4390-88e6-5802aea6305e/keyword?word='productivity'
curl -X PUT localhost:8080/content/c37bad98-b622-4390-88e6-5802aea6305e/keyword?word='work-life'
