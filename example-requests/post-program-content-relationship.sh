#!/bin/bash

curl -X POST -H "Content-Type: application/json" \
             -d '{"title": "Liquid Tension", "isFree": true, "isChildFriendly": true, "rightsOwner": "Sony Records", "duration": 200}' \
             localhost:8080/album/a0d984bf-b245-44f5-a142-d2f13b1f8a5b/song

curl -X POST -H "Content-Type: application/json" \
             -d '{"title": "Mechanistic Flow", "isFree": false, "isChildFriendly": true, "rightsOwner": "Sony Records", "duration": 310}' \
             localhost:8080/album/a0d984bf-b245-44f5-a142-d2f13b1f8a5b/song

curl -X POST -H "Content-Type: application/json" \
             -d '{"title": "Inauguration episode", "isFree": true, "isChildFriendly": true, "description": "First epidose of our show", "duration": 2700}' \
             localhost:8080/show/7f0310bd-1b12-4ecb-b39f-c5eb30f6b59a/episode

curl -X POST -H "Content-Type: application/json" \
             -d '{"title": "Productivity Books", "isFree": false, "isChildFriendly": true, "description": "Best books for productivity", "duration": 3680}' \
             localhost:8080/show/7f0310bd-1b12-4ecb-b39f-c5eb30f6b59a/episode
