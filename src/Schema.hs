{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE OverloadedStrings #-}


module Schema
  ( createConstraints
  ) where

import           Control.Monad.Catch            ( MonadCatch
                                                , MonadThrow
                                                )
import           Control.Monad.IO.Class         ( MonadIO )
import           Data                           ( printQuery
                                                , runDB
                                                , showToText
                                                )
import           Database.Bolt                  ( Pipe
                                                , props
                                                , queryP_
                                                )
import           Types                          ( ContentListLabel(UnitList)
                                                , ContentUnitLabel(Unit)
                                                , LikeLabel(Like)
                                                , ProgramLabel(ContentProgram)
                                                , UserLabel(AppUser)
                                                )


createConstraints :: (MonadIO m, MonadThrow m, MonadCatch m) => Pipe -> m ()
createConstraints pipe = do
  mapM_
    (createIndex pipe)
    [ userIdxC
    , programIdxC
    , contentIdxC
    , listIdxC
    , likeUserC
    , userNicknameC
    , userEmailC
    , programNameC
    ]
 where
  createIndex pipe' qr = runDB pipe' $ queryP_ qr (props []) >> printQuery qr
  --
  userIdxC =
    "CREATE CONSTRAINT ON (u:"
      <> showToText AppUser
      <> ")"
      <> " ASSERT u.idx IS UNIQUE"
  programIdxC =
    "CREATE CONSTRAINT ON (p:"
      <> showToText ContentProgram
      <> ")"
      <> " ASSERT p.idx IS UNIQUE"
  contentIdxC =
    "CREATE CONSTRAINT ON (c:"
      <> showToText Unit
      <> ")"
      <> " ASSERT c.idx IS UNIQUE"
  listIdxC =
    "CREATE CONSTRAINT ON (l:"
      <> showToText UnitList
      <> ")"
      <> " ASSERT l.idx IS UNIQUE"
  likeUserC =
    "CREATE CONSTRAINT ON (l:"
      <> showToText Like
      <> ")"
      <> " ASSERT (l.idx, l.user_idx) IS NODE KEY"
  userNicknameC =
    "CREATE CONSTRAINT ON (u:"
      <> showToText AppUser
      <> ")"
      <> " ASSERT u.nickname IS UNIQUE"
  userEmailC =
    "CREATE CONSTRAINT ON (u:"
      <> showToText AppUser
      <> ")"
      <> " ASSERT u.email IS UNIQUE"
  programNameC =
    "CREATE CONSTRAINT ON (p:"
      <> showToText ContentProgram
      <> ")"
      <> " ASSERT p.name IS UNIQUE"
