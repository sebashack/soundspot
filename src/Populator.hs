{-# LANGUAGE OverloadedStrings    #-}
{-# LANGUAGE FlexibleContexts     #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Populator
  ( runPopulator
  , createAlbumSongsSample
  , createAppUserSample
  , createMusicianAlbumSamples
  , createMusicianSample
  , createPodcasterSample
  , createPodcasterShowSamples
  , createShowEpisodesSample
  , createEpisodeKeywordSamples
  , createSongKeywordSamples
  , createSongFtArtistsSamples
  , createEpisodeParticipantsSamples
  , createPodcastListSamples
  , createMusicListSamples
  , createListSongsSample
  , createListEpisodesSample
  , createEpisodeLikeSamples
  , createSongLikeSamples
  , concatContentIds
  ) where

import           Control.Monad                  ( void )
import           Control.Monad.Catch            ( handle )
import           Control.Monad.IO.Class         ( liftIO )
import           Control.Monad.Reader           ( ReaderT
                                                , runReaderT
                                                )
import           Data.Bool                      ( bool )
import           Data.Maybe                     ( catMaybes )
import qualified Data.Text                     as T
import           Data.Text                      ( Text )
import           Data.Time.Calendar             ( Day
                                                , fromGregorian
                                                )
import           Data.UUID                      ( UUID )
import           Database.Bolt                  ( BoltError )
import           Env                            ( Env )
import           Test.QuickCheck                ( Arbitrary(arbitrary)
                                                , Gen
                                                , chooseInt
                                                , generate
                                                , oneof
                                                , resize
                                                , shuffle
                                                , sublistOf
                                                , vectorOf
                                                )


import           Data                           ( createContentCreatorProgramRelationship
                                                , createContentList
                                                , createContentListUnitRelationship
                                                , createContentUnitKeywordRelationship
                                                , createContributorContentUnitRelationship
                                                , createProgramContentUnitRelationship
                                                , createUser
                                                , createUserLike
                                                )
import           Types                          ( CCProgRelationShip
                                                  ( FEATURES_IN
                                                  , HOSTS
                                                  , PARTICIPATES_IN
                                                  , RECORDED
                                                  )
                                                , ContentListLabel
                                                  ( MusicList
                                                  , PodcastList
                                                  )
                                                , ContentUnitLabel
                                                  ( Episode
                                                  , Song
                                                  )
                                                , ContentUnitPostRequest
                                                , LikeLabel
                                                  ( EpisodeLike
                                                  , SongLike
                                                  )
                                                , ProgramLabel
                                                  ( MusicAlbum
                                                  , PodcastShow
                                                  )
                                                , ProgramPostRequest
                                                , UserLabel
                                                  ( AppUser
                                                  , FtArtist
                                                  , Musician
                                                  , Participant
                                                  , Podcaster
                                                  )
                                                , UserPostReq
                                                )
import qualified Types                         as CUPR
                                                ( ContentUnitPostRequest(..) )
import qualified Types                         as UPR
                                                ( UserPostReq(..) )
import qualified Types                         as PPR
                                                ( ProgramPostRequest(..) )



type Populator = ReaderT Env IO

runPopulator :: Env -> Populator a -> IO a
runPopulator env p = runReaderT p $ env

-- Content Likes
createEpisodeLikeSamples :: Int -> [UUID] -> [UUID] -> Populator ()
createEpisodeLikeSamples n = createContentLikeSamples n EpisodeLike

createSongLikeSamples :: Int -> [UUID] -> [UUID] -> Populator ()
createSongLikeSamples n = createContentLikeSamples n SongLike

createContentLikeSamples
  :: Int -> LikeLabel -> [UUID] -> [UUID] -> Populator ()
createContentLikeSamples n lLabel userIds contentIds = mapM_ g userIds
  where g userId = createContentLikeSample n lLabel userId contentIds

createContentLikeSample :: Int -> LikeLabel -> UUID -> [UUID] -> Populator ()
createContentLikeSample n lLabel userId contentIds = do
  randomIds <- liftIO generator
  mapM_ createLike randomIds
 where
  generator :: IO [UUID]
  generator = generate $ uuidPickerGen n contentIds
  --
  cLabel    = case lLabel of
    SongLike    -> Song
    EpisodeLike -> Episode
    _           -> error "Impossible case"
  --
  createLike :: UUID -> Populator ()
  createLike contentId =
    handle onError (createUserLike cLabel lLabel userId contentId)
  --
  onError :: BoltError -> Populator ()
  onError _ = pure ()


-- List Content
createListSongsSample :: Int -> [(UUID, [UUID])] -> [UUID] -> Populator ()
createListSongsSample n = createListContentSamples n MusicList

createListEpisodesSample :: Int -> [(UUID, [UUID])] -> [UUID] -> Populator ()
createListEpisodesSample n = createListContentSamples n PodcastList

createListContentSamples
  :: Int -> ContentListLabel -> [(UUID, [UUID])] -> [UUID] -> Populator ()
createListContentSamples n lLabel userToLists contentIds = mapM_ f userToLists
 where
  f :: (UUID, [UUID]) -> Populator ()
  f (_, listIds) = mapM_ g listIds
  --
  g listId = createListContentSample n lLabel listId contentIds

createListContentSample
  :: Int -> ContentListLabel -> UUID -> [UUID] -> Populator ()
createListContentSample n lLabel listId contentIds = do
  randomIds <- liftIO generator
  mapM_ createConRl' randomIds
 where
  generator :: IO [UUID]
  generator = generate $ uuidPickerGen n contentIds
  --
  cLabel    = case lLabel of
    MusicList   -> Song
    PodcastList -> Episode
    _           -> error "Impossible case"
  --
  createConRl' :: UUID -> Populator ()
  createConRl' contentId = handle
    onError
    (createContentListUnitRelationship lLabel cLabel listId contentId)
  --
  onError :: BoltError -> Populator ()
  onError _ = pure ()

concatContentIds :: [(UUID, [(UUID, [UUID])])] -> [UUID]
concatContentIds creatorToProgToCont =
  concat $ concat . (fmap snd) . snd <$> creatorToProgToCont


-- Lists
createPodcastListSamples :: Int -> [UUID] -> Populator [(UUID, [UUID])]
createPodcastListSamples n = createUserListSamples n PodcastList

createMusicListSamples :: Int -> [UUID] -> Populator [(UUID, [UUID])]
createMusicListSamples n = createUserListSamples n MusicList

createUserListSamples
  :: Int -> ContentListLabel -> [UUID] -> Populator [(UUID, [UUID])]
createUserListSamples n lLabel = mapM (createUserListSample n lLabel)

createUserListSample
  :: Int -> ContentListLabel -> UUID -> Populator (UUID, [UUID])
createUserListSample n lLabel userId = do
  listPostReqs <- liftIO generator
  maybeUuids   <- mapM createProgRl' listPostReqs
  return $ (userId, catMaybes maybeUuids)
 where
  generator :: IO [Text]
  generator = generate $ do
    k <- chooseInt (1, n)
    case lLabel of
      MusicList   -> vectorOf k musicListNameGen
      PodcastList -> vectorOf k podcastListNameGen
      _           -> error "Impossible case"
  --
  createProgRl' :: Text -> Populator (Maybe UUID)
  createProgRl' listName =
    handle onError (Just <$> createContentList userId lLabel listName)
  --
  onError :: BoltError -> Populator (Maybe UUID)
  onError _ = pure Nothing


-- Contributor
createSongFtArtistsSamples :: Int -> [(UUID, [(UUID, [UUID])])] -> Populator ()
createSongFtArtistsSamples n = createContentContributorSamples n Song

createEpisodeParticipantsSamples
  :: Int -> [(UUID, [(UUID, [UUID])])] -> Populator ()
createEpisodeParticipantsSamples n = createContentContributorSamples n Episode

createContentContributorSamples
  :: Int -> ContentUnitLabel -> [(UUID, [(UUID, [UUID])])] -> Populator ()
createContentContributorSamples n cLabel = mapM_ g
 where
  g :: (UUID, [(UUID, [UUID])]) -> Populator ()
  g (_, programToContent) =
    let f (_, contentIds) =
          mapM_ (createContentContributors n cLabel) contentIds
    in  mapM_ f programToContent

createContentContributors :: Int -> ContentUnitLabel -> UUID -> Populator ()
createContentContributors n cLabel contentId = do
  userPostReqs <- liftIO $ generator n
  mapM_ createContrib' userPostReqs
 where
  generator :: Int -> IO [UserPostReq]
  generator    = sampleEntitiesGe False
  --
  (uLabel, rl) = case cLabel of
    Song    -> (FtArtist, FEATURES_IN)
    Episode -> (Participant, PARTICIPATES_IN)
    _       -> error "Impossible case"
  --
  createContrib' :: UserPostReq -> Populator ()
  createContrib' req = handle
    onError
    (void $ createContributorContentUnitRelationship uLabel
                                                     cLabel
                                                     rl
                                                     contentId
                                                     req
    )
  --
  onError :: BoltError -> Populator ()
  onError _ = pure ()


-- Content keywords
createSongKeywordSamples :: [(UUID, [(UUID, [UUID])])] -> Populator ()
createSongKeywordSamples = createContentKeywordSamples Song

createEpisodeKeywordSamples :: [(UUID, [(UUID, [UUID])])] -> Populator ()
createEpisodeKeywordSamples = createContentKeywordSamples Episode

createContentKeywordSamples
  :: ContentUnitLabel -> [(UUID, [(UUID, [UUID])])] -> Populator ()
createContentKeywordSamples cLabel = mapM_ g
 where
  g :: (UUID, [(UUID, [UUID])]) -> Populator ()
  g (_, programToContent) =
    let f (_, contentIds) = mapM_ (createContentKeywords cLabel) contentIds
    in  mapM_ f programToContent

createContentKeywords :: ContentUnitLabel -> UUID -> Populator ()
createContentKeywords cLabel contentId = do
  keywords <- liftIO generator
  mapM_ createConRl' keywords
 where
  generator = case cLabel of
    Song    -> generate songKeywordsGen
    Episode -> generate episodeKeywordsGen
    _       -> error "Impossible case"
  --
  createConRl' :: Text -> Populator ()
  createConRl' kw =
    handle onError (createContentUnitKeywordRelationship contentId kw)
  --
  onError :: BoltError -> Populator ()
  onError _ = pure ()


-- Countent Units
createAlbumSongsSample
  :: Int -> [(UUID, [UUID])] -> Populator [(UUID, [(UUID, [UUID])])]
createAlbumSongsSample n = createProgramContentSamples n MusicAlbum Song

createShowEpisodesSample
  :: Int -> [(UUID, [UUID])] -> Populator [(UUID, [(UUID, [UUID])])]
createShowEpisodesSample n = createProgramContentSamples n PodcastShow Episode

createProgramContentSamples
  :: Int
  -> ProgramLabel
  -> ContentUnitLabel
  -> [(UUID, [UUID])]
  -> Populator [(UUID, [(UUID, [UUID])])]
createProgramContentSamples n pLabel cLabel userToPrograms = mapM
  f
  userToPrograms
 where
  f :: (UUID, [UUID]) -> Populator (UUID, [(UUID, [UUID])])
  f (userId, programIds) = do
    progToContent <- mapM g programIds
    return (userId, progToContent)
  --
  g = createProgramContentSample n pLabel cLabel

createProgramContentSample
  :: Int -> ProgramLabel -> ContentUnitLabel -> UUID -> Populator (UUID, [UUID])
createProgramContentSample n pLabel cLabel programId = do
  conPostReqs <- liftIO $ generator n
  maybeUuids  <- mapM createConRl' conPostReqs
  return $ (programId, catMaybes maybeUuids)
 where
  generator :: Int -> IO [ContentUnitPostRequest]
  generator = sampleEntitiesGe True
  --
  createConRl' :: ContentUnitPostRequest -> Populator (Maybe UUID)
  createConRl' req = handle
    onError
    (Just <$> createProgramContentUnitRelationship pLabel cLabel programId req)
  --
  onError :: BoltError -> Populator (Maybe UUID)
  onError _ = pure Nothing


-- Programs
createPodcasterShowSamples :: Int -> [UUID] -> Populator [(UUID, [UUID])]
createPodcasterShowSamples n userIds =
  createUserProgramSamples n Podcaster PodcastShow HOSTS userIds

createMusicianAlbumSamples :: Int -> [UUID] -> Populator [(UUID, [UUID])]
createMusicianAlbumSamples n userIds =
  createUserProgramSamples n Musician MusicAlbum RECORDED userIds

createUserProgramSamples
  :: Int
  -> UserLabel
  -> ProgramLabel
  -> CCProgRelationShip
  -> [UUID]
  -> Populator [(UUID, [UUID])]
createUserProgramSamples n uLabel pLabel rl userIds =
  mapM (createUserProgramSample n uLabel pLabel rl) userIds

createUserProgramSample
  :: Int
  -> UserLabel
  -> ProgramLabel
  -> CCProgRelationShip
  -> UUID
  -> Populator (UUID, [UUID])
createUserProgramSample n uLabel pLabel rl userId = do
  progPostReqs <- liftIO $ generator n
  maybeUuids   <- mapM createProgRl' progPostReqs
  return $ (userId, catMaybes maybeUuids)
 where
  generator :: Int -> IO [ProgramPostRequest]
  generator = sampleEntitiesGe True
  --
  createProgRl' :: ProgramPostRequest -> Populator (Maybe UUID)
  createProgRl' req = handle
    onError
    (   Just
    <$> createContentCreatorProgramRelationship uLabel pLabel rl userId req
    )
  --
  onError :: BoltError -> Populator (Maybe UUID)
  onError _ = pure Nothing

-- Users
createMusicianSample :: Int -> Populator [UUID]
createMusicianSample = createUserSample Musician

createPodcasterSample :: Int -> Populator [UUID]
createPodcasterSample = createUserSample Podcaster

createAppUserSample :: Int -> Populator [UUID]
createAppUserSample = createUserSample AppUser

createUserSample :: UserLabel -> Int -> Populator [UUID]
createUserSample label n = do
  userPostReqs <- liftIO $ generator n
  maybeUuids   <- mapM createUser' userPostReqs
  return $ catMaybes maybeUuids
 where
  generator :: Int -> IO [UserPostReq]
  generator = sampleEntitiesGe True
  --
  createUser' :: UserPostReq -> Populator (Maybe UUID)
  createUser' req = handle onError (Just <$> createUser label req)
  --
  onError :: BoltError -> Populator (Maybe UUID)
  onError _ = pure Nothing


-- General sampler
sampleEntitiesGe :: Arbitrary a => Bool -> Int -> IO [a]
sampleEntitiesGe isExact n = go n []
 where
  generator k = resize k arbitrary
  --
  go u accum = do
    if length accum >= n || u < 1
      then bool (return accum) (return $ take n accum) isExact
      else do
        samples <- generate $ generator u
        go (u `div` 5) (samples <> accum)

-- Arbitrary instances
instance Arbitrary UserPostReq where
  arbitrary = do
    firstName          <- firstNameGen
    lastName           <- lastNameGen
    nickname           <- nicknameGen firstName lastName
    email              <- emailGen firstName lastName
    lang               <- languageGen
    (country, capital) <- countryAndCapitalGen
    birthdate          <- dayGen
    return UPR.UserPostReq { UPR.firstName         = firstName
                           , UPR.lastName          = lastName
                           , UPR.email             = email
                           , UPR.nickname          = nickname
                           , UPR.preferredLanguage = lang
                           , UPR.country           = country
                           , UPR.city              = capital
                           , UPR.birthdate         = birthdate
                           }

instance Arbitrary ProgramPostRequest where
  arbitrary = do
    name           <- titleGenerator
    description    <- descriptionGen
    coverImageLink <- pure "https://somecdn.com"
    releaseDate    <- dayGen
    return PPR.ProgramPostRequest { PPR.name           = name
                                  , PPR.description    = description
                                  , PPR.coverImageLink = coverImageLink
                                  , PPR.releaseDate    = releaseDate
                                  }


instance Arbitrary ContentUnitPostRequest where
  arbitrary = do
    isFree          <- arbitrary
    isChildFriendly <- arbitrary
    title           <- titleGenerator
    rightsOwner     <- oneof ((pure . Just) <$> recordingStudios)
    description     <- oneof [Just <$> descriptionGen, pure Nothing]
    duration        <- chooseInt (60, 600)
    return CUPR.ContentUnitPostRequest { CUPR.isFree          = isFree
                                       , CUPR.isChildFriendly = isChildFriendly
                                       , CUPR.title           = title
                                       , CUPR.rightsOwner     = rightsOwner
                                       , CUPR.description     = description
                                       , CUPR.duration = fromIntegral duration
                                       }
   where
    recordingStudios =
      [ "Sony Records"
      , "Air Studios"
      , "Capitol Studios"
      , "Eastwest Studios"
      , "Electric Lady Studios"
      , "Germano Studios"
      , "Realwordl Studios"
      , "Ocean Way"
      , "Metropolis Studios"
      , "Record Plant"
      , "Sterling Sound"
      ]



-- Helpers
uuidPickerGen :: Int -> [UUID] -> Gen [UUID]
uuidPickerGen n uuids = do
  k  <- chooseInt (1, n)
  ls <- shuffle uuids
  return $ take k ls

musicListNameGen :: Gen Text
musicListNameGen = do
  part1 <- oneof adjs
  part2 <- oneof names
  return $ part1 <> " " <> part2
 where
  adjs =
    pure
      <$> [ "Favorite"
          , "Romantic"
          , "Relaxing"
          , "Study"
          , "Hard"
          , "Chill"
          , "Meditation"
          , "Working-out"
          ]
  names = pure <$> ["music", "songs", "tracks", "hits"]

podcastListNameGen :: Gen Text
podcastListNameGen = do
  part1 <- oneof adjs
  part2 <- oneof names
  return $ part1 <> " " <> part2
 where
  adjs =
    pure
      <$> [ "Favorite"
          , "Productivity"
          , "Spirituality"
          , "Most important"
          , "Comedy"
          , "Philosophical"
          , "Intellectual"
          , "Motivational"
          ]
  names = pure <$> ["shows", "episodes", "conversations"]

songKeywordsGen :: Gen [Text]
songKeywordsGen = do
  ks <- shuffle keywords
  n  <- chooseInt (1, length ks `div` 4)
  return $ take n ks
 where
  keywords =
    [ "acid-jazz"
    , "alternative"
    , "blues"
    , "blues-and-rhythm"
    , "classical"
    , "country"
    , "cyber-punk"
    , "electronic"
    , "experimental"
    , "funk"
    , "fusion-jazz"
    , "fusion-rock"
    , "hard-rock"
    , "hip-hop"
    , "indie"
    , "jazz"
    , "latin-jazz"
    , "meditation"
    , "neo-punk"
    , "pop"
    , "punk"
    , "rap"
    , "rock"
    , "salsa"
    , "son-cubano"
    , "soul"
    , "vaporwave"
    ]

episodeKeywordsGen :: Gen [Text]
episodeKeywordsGen = do
  ks <- shuffle keywords
  n  <- chooseInt (1, length ks `div` 4)
  return $ take n ks
 where
  keywords =
    [ "anthropology"
    , "arts"
    , "beauty"
    , "business"
    , "children"
    , "comedy"
    , "economy"
    , "engineering"
    , "family"
    , "fashion"
    , "finances"
    , "health-and-fitness"
    , "maternity"
    , "meditation"
    , "movies"
    , "news"
    , "philosophy"
    , "politics"
    , "pop-culture"
    , "pregnancy"
    , "productivity"
    , "real-life-stories"
    , "religion"
    , "science"
    , "software"
    , "spirituality"
    , "sports"
    , "technology"
    ]


firstNameGen :: Gen Text
firstNameGen = oneof (pure <$> firstNames)

lastNameGen :: Gen Text
lastNameGen = oneof (pure <$> lastNames)

emailGen :: Text -> Text -> Gen Text
emailGen fName lName = do
  rText <- randomAlphaNumTextGen 4
  n     <- chooseInt (0, 100)
  let prefix =
        T.toLower fName
          <> "."
          <> T.toLower lName
          <> "."
          <> rText
          <> "."
          <> (T.pack . show) n
  suffix <- oneof
    (   pure
    <$> ["hotmail.com", "gmail.com", "aws.com", "everest.net", "icloud.com"]
    )
  return $ prefix <> "@" <> suffix

nicknameGen :: Text -> Text -> Gen Text
nicknameGen fName lName = do
  fPart <- subTextGen fName
  lPart <- subTextGen lName
  rText <- randomAlphaNumTextGen 4
  n     <- chooseInt (0, 1000)
  return $ fPart <> lPart <> (T.pack . show) n <> rText

subTextGen :: Text -> Gen Text
subTextGen txt = T.pack <$> (sublistOf $ T.unpack txt)

randomAlphaNumTextGen :: Int -> Gen Text
randomAlphaNumTextGen n = do
  let chars = ['A' .. 'Z'] <> ['a' .. 'z'] <> ['0' .. '9']
  chars' <- shuffle chars
  return $ T.pack $ take n chars'

countryAndCapitalGen :: Gen (Text, Text)
countryAndCapitalGen = do
  countryAndCapital <- oneof (pure <$> countriesAndCapitals)
  let [country, capital] = T.splitOn "," countryAndCapital
  return (country, capital)

languageGen :: Gen Text
languageGen = oneof (pure <$> languages)

dayGen :: Gen Day
dayGen = do
  year  <- chooseInt (1979, 2007)
  month <- chooseInt (1, 12)
  day   <- chooseInt (1, 28)
  return $ fromGregorian (fromIntegral year) month day

titleGenerator :: Gen Text
titleGenerator = do
  noun      <- nounGen
  numeral   <- oneof (pure <$> ["", " I", " II", " III", " IV", " V"])
  adjective <- adjectiveGen
  return $ noun <> " " <> adjective <> numeral

descriptionGen :: Gen Text
descriptionGen = do
  noun      <- nounGen
  adjective <- adjectiveGen
  return $ "It's a " <> adjective <> " " <> noun <> "."

nounGen :: Gen Text
nounGen = oneof (pure <$> nouns)

adjectiveGen :: Gen Text
adjectiveGen = oneof (pure <$> adjectives)


nouns :: [Text]
nouns =
  [ "payout"
  , "situation"
  , "gear"
  , "geek"
  , "body"
  , "courtroom"
  , "trivia"
  , "bowel"
  , "bio"
  , "realization"
  , "semblance"
  , "mutiny"
  , "highway"
  , "designer"
  , "kale"
  , "infraction"
  , "jaw"
  , "reception"
  , "cognition"
  , "oath"
  , "smuggling"
  , "ambition"
  , "compliance"
  , "pulpit"
  , "coexistence"
  , "boob"
  , "concrete"
  , "lung"
  , "dear"
  , "happening"
  , "penetration"
  , "surcharge"
  , "bonfire"
  , "chinese"
  , "caucus"
  , "grind"
  , "boiler"
  , "station"
  , "request"
  , "infighting"
  , "bloc"
  , "awe"
  , "quarter"
  , "kidding"
  , "agent"
  , "showdown"
  , "insulin"
  , "background"
  , "celebration"
  , "importer"
  ]

adjectives :: [Text]
adjectives =
  [ "haitian"
  , "attributable"
  , "fluent"
  , "free-trade"
  , "heartbroken"
  , "longtime"
  , "allergic"
  , "incidental"
  , "sleepless"
  , "rainy"
  , "reasoned"
  , "four-year-old"
  , "unimpressed"
  , "paralyzed"
  , "fertile"
  , "untenable"
  , "baltic"
  , "disgusting"
  , "unbearable"
  , "man-made"
  , "silky"
  , "bipartisan"
  , "introspective"
  , "open-ended"
  , "stringy"
  , "thriving"
  , "separating"
  , "outright"
  , "self-centered"
  , "faith-based"
  , "pathetic"
  , "hurried"
  , "ancillary"
  , "slow"
  , "cool"
  , "fourth-grade"
  , "right-wing"
  , "oncoming"
  , "iranian"
  , "cardinal"
  , "overcast"
  , "flourishing"
  , "fellow"
  , "provisional"
  , "forced"
  , "dynamic"
  , "dormant"
  , "vile"
  , "extensive"
  , "migratory"
  ]


firstNames :: [Text]
firstNames =
  [ "Abigail"
  , "Alexandra"
  , "Alison"
  , "Amanda"
  , "Amelia"
  , "Amy"
  , "Andrea"
  , "Angela"
  , "Anna"
  , "Anne"
  , "Audrey"
  , "Ava"
  , "Bella"
  , "Bernadette"
  , "Carol"
  , "Caroline"
  , "Carolyn"
  , "Chloe"
  , "Claire"
  , "Deirdre"
  , "Diana"
  , "Diane"
  , "Donna"
  , "Dorothy"
  , "Elizabeth"
  , "Ella"
  , "Emily"
  , "Emma"
  , "Faith"
  , "Felicity"
  , "Fiona"
  , "Gabrielle"
  , "Grace"
  , "Hannah"
  , "Heather"
  , "Irene"
  , "Jan"
  , "Jane"
  , "Jasmine"
  , "Jennifer"
  , "Jessica"
  , "Joan"
  , "Joanne"
  , "Julia"
  , "Karen"
  , "Katherine"
  , "Kimberly"
  , "Kylie"
  , "Lauren"
  , "Leah"
  , "Lillian"
  , "Lily"
  , "Lisa"
  , "Madeleine"
  , "Maria"
  , "Mary"
  , "Megan"
  , "Melanie"
  , "Michelle"
  , "Molly"
  , "Natalie"
  , "Nicola"
  , "Olivia"
  , "Penelope"
  , "Pippa"
  , "Rachel"
  , "Rebecca"
  , "Rose"
  , "Ruth"
  , "Sally"
  , "Samantha"
  , "Sarah"
  , "Sonia"
  , "Sophie"
  , "Stephanie"
  , "Sue"
  , "Theresa"
  , "Tracey"
  , "Una"
  , "Vanessa"
  , "Victoria"
  , "Virginia"
  , "Wanda"
  , "Wendy"
  , "Yvonne"
  , "Zoe"
  , "Adam"
  , "Adrian"
  , "Alan"
  , "Alexander"
  , "Andrew"
  , "Anthony"
  , "Austin"
  , "Benjamin"
  , "Blake"
  , "Boris"
  , "Brandon"
  , "Brian"
  , "Cameron"
  , "Carl"
  , "Charles"
  , "Christian"
  , "Christopher"
  , "Colin"
  , "Connor"
  , "Dan"
  , "David"
  , "Dominic"
  , "Dylan"
  , "Edward"
  , "Eric"
  , "Evan"
  , "Frank"
  , "Gavin"
  , "Gordon"
  , "Harry"
  , "Ian"
  , "Isaac"
  , "Jack"
  , "Jacob"
  , "Jake"
  , "James"
  , "Jason"
  , "Joe"
  , "John"
  , "Jonathan"
  , "Joseph"
  , "Joshua"
  , "Julian"
  , "Justin"
  , "Keith"
  , "Kevin"
  , "Leonard"
  , "Liam"
  , "Lucas"
  , "Luke"
  , "Matt"
  , "Max"
  , "Michael"
  , "Nathan"
  , "Neil"
  , "Nicholas"
  , "Oliver"
  , "Owen"
  , "Paul"
  , "Peter"
  , "Phil"
  , "Piers"
  , "Richard"
  , "Robert"
  , "Ryan"
  , "Sam"
  , "Sean"
  , "Sebastian"
  , "Simon"
  , "Stephen"
  , "Steven"
  , "Stewart"
  , "Thomas"
  , "Tim"
  , "Trevor"
  , "Victor"
  , "Warren"
  , "William"
  ]

lastNames :: [Text]
lastNames =
  [ "Abraham"
  , "Allan"
  , "Alsop"
  , "Anderson"
  , "Arnold"
  , "Avery"
  , "Bailey"
  , "Baker"
  , "Ball"
  , "Bell"
  , "Berry"
  , "Black"
  , "Blake"
  , "Bond"
  , "Bower"
  , "Brown"
  , "Buckland"
  , "Burgess"
  , "Butler"
  , "Cameron"
  , "Campbell"
  , "Carr"
  , "Chapman"
  , "Churchill"
  , "Clark"
  , "Clarkson"
  , "Coleman"
  , "Cornish"
  , "Davidson"
  , "Davies"
  , "Dickens"
  , "Dowd"
  , "Duncan"
  , "Dyer"
  , "Edmunds"
  , "Ellison"
  , "Ferguson"
  , "Fisher"
  , "Forsyth"
  , "Fraser"
  , "Gibson"
  , "Gill"
  , "Glover"
  , "Graham"
  , "Grant"
  , "Gray"
  , "Greene"
  , "Hamilton"
  , "Hardacre"
  , "Harris"
  , "Hart"
  , "Hemmings"
  , "Henderson"
  , "Hill"
  , "Hodges"
  , "Howard"
  , "Hudson"
  , "Hughes"
  , "Hunter"
  , "Ince"
  , "Jackson"
  , "James"
  , "Johnston"
  , "Jones"
  , "Kelly"
  , "Kerr"
  , "King"
  , "Knox"
  , "Lambert"
  , "Langdon"
  , "Lawrence"
  , "Lee"
  , "Lewis"
  , "Lyman"
  , "MacDonald"
  , "Mackay"
  , "Mackenzie"
  , "MacLeod"
  , "Manning"
  , "Marshall"
  , "Martin"
  , "Mathis"
  , "May"
  , "McDonald"
  , "McLean"
  , "McGrath"
  , "Metcalfe"
  , "Miller"
  , "Mills"
  , "Mitchell"
  , "Morgan"
  , "Morrison"
  , "Murray"
  , "Nash"
  , "Newman"
  , "Nolan"
  , "North"
  , "Ogden"
  , "Oliver"
  , "Paige"
  , "Parr"
  , "Parsons"
  , "Paterson"
  , "Payne"
  , "Peake"
  , "Peters"
  , "Piper"
  , "Poole"
  , "Powell"
  , "Pullman"
  , "Quinn"
  , "Rampling"
  , "Randall"
  , "Rees"
  , "Reid"
  , "Roberts"
  , "Robertson"
  , "Ross"
  , "Russell"
  , "Rutherford"
  , "Sanderson"
  , "Scott"
  , "Sharp"
  , "Short"
  , "Simpson"
  , "Skinner"
  , "Slater"
  , "Smith"
  , "Springer"
  , "Stewart"
  , "Sutherland"
  , "Taylor"
  , "Terry"
  , "Thomson"
  , "Tucker"
  , "Turner"
  , "Underwood"
  , "Vance"
  , "Vaughan"
  , "Walker"
  , "Wallace"
  , "Walsh"
  , "Watson"
  , "Welch"
  , "White"
  , "Wilkins"
  , "Wilson"
  , "Wright"
  , "Young"
  ]

languages :: [Text]
languages =
  [ "Language"
  , "Afrikanns"
  , "Albanian"
  , "Arabic"
  , "Armenian"
  , "Basque"
  , "Bengali"
  , "Bulgarian"
  , "Catalan"
  , "Cambodian"
  , "Chinese (Mandarin)"
  , "Croation"
  , "Czech"
  , "Danish"
  , "Dutch"
  , "English"
  , "Estonian"
  , "Fiji"
  , "Finnish"
  , "French"
  , "Georgian"
  , "German"
  , "Greek"
  , "Gujarati"
  , "Hebrew"
  , "Hindi"
  , "Hungarian"
  , "Icelandic"
  , "Indonesian"
  , "Irish"
  , "Italian"
  , "Japanese"
  , "Javanese"
  , "Korean"
  , "Latin"
  , "Latvian"
  , "Lithuanian"
  , "Macedonian"
  , "Malay"
  , "Malayalam"
  , "Maltese"
  , "Maori"
  , "Marathi"
  , "Mongolian"
  , "Nepali"
  , "Norwegian"
  , "Persian"
  , "Polish"
  , "Portuguese"
  , "Punjabi"
  , "Quechua"
  , "Romanian"
  , "Russian"
  , "Samoan"
  , "Serbian"
  , "Slovak"
  , "Slovenian"
  , "Spanish"
  , "Swahili"
  , "Swedish"
  , "Tamil"
  , "Tatar"
  , "Telugu"
  , "Thai"
  , "Tibetan"
  , "Tonga"
  , "Turkish"
  , "Ukranian"
  , "Urdu"
  , "Uzbek"
  , "Vietnamese"
  , "Welsh"
  , "Xhosa"
  ]

countriesAndCapitals :: [Text]
countriesAndCapitals =
  [ "Palestine,Jerusalem"
  , "Nauru,Yaren"
  , "Saint Martin,Marigot"
  , "Tokelau,Atafu"
  , "Afghanistan,Kabul"
  , "Albania,Tirana"
  , "Algeria,Algiers"
  , "American Samoa,Pago Pago"
  , "Andorra,Andorra la Vella"
  , "Angola,Luanda"
  , "Anguilla,The Valley"
  , "Antigua and Barbuda,Saint John's"
  , "Argentina,Buenos Aires"
  , "Armenia,Yerevan"
  , "Aruba,Oranjestad"
  , "Australia,Canberra"
  , "Austria,Vienna"
  , "Azerbaijan,Baku"
  , "Bahamas,Nassau"
  , "Bahrain,Manama"
  , "Bangladesh,Dhaka"
  , "Barbados,Bridgetown"
  , "Belarus,Minsk"
  , "Belgium,Brussels"
  , "Belize,Belmopan"
  , "Benin,Porto-Novo"
  , "Bermuda,Hamilton"
  , "Bhutan,Thimphu"
  , "Bolivia,La Paz"
  , "Bosnia and Herzegovina,Sarajevo"
  , "Botswana,Gaborone"
  , "Brazil,Brasilia"
  , "British Virgin Islands,Road Town"
  , "Brunei Darussalam,Bandar Seri Begawan"
  , "Bulgaria,Sofia"
  , "Burkina Faso,Ouagadougou"
  , "Myanmar,Rangoon"
  , "Burundi,Bujumbura"
  , "Cambodia,Phnom Penh"
  , "Cameroon,Yaounde"
  , "Canada,Ottawa"
  , "Cape Verde,Praia"
  , "Cayman Islands,George Town"
  , "Central African Republic,Bangui"
  , "Chad,N'Djamena"
  , "Chile,Santiago"
  , "China,Beijing"
  , "Christmas Island,The Settlement"
  , "Cocos Islands,West Island"
  , "Colombia,Bogota"
  , "Comoros,Moroni"
  , "Democratic Republic of the Congo,Kinshasa"
  , "Republic of Congo,Brazzaville"
  , "Cook Islands,Avarua"
  , "Costa Rica,San Jose"
  , "Cote d'Ivoire,Yamoussoukro"
  , "Croatia,Zagreb"
  , "Cuba,Havana"
  , "Curaçao,Willemstad"
  , "Cyprus,Nicosia"
  , "Czech Republic,Prague"
  , "Denmark,Copenhagen"
  , "Djibouti,Djibouti"
  , "Dominica,Roseau"
  , "Dominican Republic,Santo Domingo"
  , "Ecuador,Quito"
  , "Egypt,Cairo"
  , "El Salvador,San Salvador"
  , "Equatorial Guinea,Malabo"
  , "Eritrea,Asmara"
  , "Estonia,Tallinn"
  , "Ethiopia,Addis Ababa"
  , "Falkland Islands,Stanley"
  , "Faroe Islands,Torshavn"
  , "Fiji,Suva"
  , "Finland,Helsinki"
  , "France,Paris"
  , "French Polynesia,Papeete"
  , "Gabon,Libreville"
  , "The Gambia,Banjul"
  , "Georgia,Tbilisi"
  , "Germany,Berlin"
  , "Ghana,Accra"
  , "Gibraltar,Gibraltar"
  , "Greece,Athens"
  , "Greenland,Nuuk"
  , "Grenada,Saint George's"
  , "Guam,Hagatna"
  , "Guatemala,Guatemala City"
  , "Guernsey,Saint Peter Port"
  , "Guinea,Conakry"
  , "Guinea-Bissau,Bissau"
  , "Guyana,Georgetown"
  , "Haiti,Port-au-Prince"
  , "Vatican City,Vatican City"
  , "Honduras,Tegucigalpa"
  , "Hungary,Budapest"
  , "Iceland,Reykjavik"
  , "India,New Delhi"
  , "Indonesia,Jakarta"
  , "Iran,Tehran"
  , "Iraq,Baghdad"
  , "Ireland,Dublin"
  , "Isle of Man,Douglas"
  , "Israel,Jerusalem"
  , "Italy,Rome"
  , "Jamaica,Kingston"
  , "Japan,Tokyo"
  , "Jersey,Saint Helier"
  , "Jordan,Amman"
  , "Kazakhstan,Astana"
  , "Kenya,Nairobi"
  , "Kiribati,Tarawa"
  , "North Korea,Pyongyang"
  , "South Korea,Seoul"
  , "Kosovo,Pristina"
  , "Kuwait,Kuwait City"
  , "Kyrgyzstan,Bishkek"
  , "Laos,Vientiane"
  , "Latvia,Riga"
  , "Lebanon,Beirut"
  , "Lesotho,Maseru"
  , "Liberia,Monrovia"
  , "Libya,Tripoli"
  , "Liechtenstein,Vaduz"
  , "Lithuania,Vilnius"
  , "Luxembourg,Luxembourg"
  , "Macedonia,Skopje"
  , "Madagascar,Antananarivo"
  , "Malawi,Lilongwe"
  , "Malaysia,Kuala Lumpur"
  , "Maldives,Male"
  , "Mali,Bamako"
  , "Malta,Valletta"
  , "Marshall Islands,Majuro"
  , "Mauritania,Nouakchott"
  , "Mauritius,Port Louis"
  , "Mexico,Mexico City"
  , "Federated States of Micronesia,Palikir"
  , "Moldova,Chisinau"
  , "Monaco,Monaco"
  , "Mongolia,Ulaanbaatar"
  , "Montenegro,Podgorica"
  , "Montserrat,Plymouth"
  , "Morocco,Rabat"
  , "Mozambique,Maputo"
  , "Namibia,Windhoek"
  , "Nepal,Kathmandu"
  , "Netherlands,Amsterdam"
  , "New Caledonia,Noumea"
  , "New Zealand,Wellington"
  , "Nicaragua,Managua"
  , "Niger,Niamey"
  , "Nigeria,Abuja"
  , "Niue,Alofi"
  , "Norfolk Island,Kingston"
  , "Northern Mariana Islands,Saipan"
  , "Norway,Oslo"
  , "Oman,Muscat"
  , "Pakistan,Islamabad"
  , "Palau,Melekeok"
  , "Panama,Panama City"
  , "Papua New Guinea,Port Moresby"
  , "Paraguay,Asuncion"
  , "Peru,Lima"
  , "Philippines,Manila"
  , "Pitcairn Islands,Adamstown"
  , "Poland,Warsaw"
  , "Portugal,Lisbon"
  , "Puerto Rico,San Juan"
  , "Qatar,Doha"
  , "Romania,Bucharest"
  , "Russia,Moscow"
  , "Rwanda,Kigali"
  , "Saint Barthelemy,Gustavia"
  , "Saint Helena,Jamestown"
  , "Saint Kitts and Nevis,Basseterre"
  , "Saint Lucia,Castries"
  , "Saint Pierre and Miquelon,Saint-Pierre"
  , "Saint Vincent and the Grenadines,Kingstown"
  , "Samoa,Apia"
  , "San Marino,San Marino"
  , "Sao Tome and Principe,Sao Tome"
  , "Saudi Arabia,Riyadh"
  , "Senegal,Dakar"
  , "Serbia,Belgrade"
  , "Seychelles,Victoria"
  , "Sierra Leone,Freetown"
  , "Singapore,Singapore"
  , "Sint Maarten,Philipsburg"
  , "Slovakia,Bratislava"
  , "Slovenia,Ljubljana"
  , "Solomon Islands,Honiara"
  , "Somalia,Mogadishu"
  , "South Africa,Pretoria"
  , "South Sudan,Juba"
  , "Spain,Madrid"
  , "Sri Lanka,Colombo"
  , "Sudan,Khartoum"
  , "Suriname,Paramaribo"
  , "Svalbard,Longyearbyen"
  , "Swaziland,Mbabane"
  , "Sweden,Stockholm"
  , "Switzerland,Bern"
  , "Syria,Damascus"
  , "Taiwan,Taipei"
  , "Tajikistan,Dushanbe"
  , "Tanzania,Dar es Salaam"
  , "Thailand,Bangkok"
  , "Timor-Leste,Dili"
  , "Togo,Lome"
  , "Tonga,Nuku'alofa"
  , "Trinidad and Tobago,Port of Spain"
  , "Tunisia,Tunis"
  , "Turkey,Ankara"
  , "Turkmenistan,Ashgabat"
  , "Turks and Caicos Islands,Grand Turk"
  , "Tuvalu,Funafuti"
  , "Uganda,Kampala"
  , "Ukraine,Kyiv"
  , "United Arab Emirates,Abu Dhabi"
  , "United Kingdom,London"
  , "United States of America,Washington"
  , "Uruguay,Montevideo"
  , "Uzbekistan,Tashkent"
  , "Vanuatu,Port-Vila"
  , "Venezuela,Caracas"
  , "Vietnam,Hanoi"
  , "US Virgin Islands,Charlotte Amalie"
  , "Wallis and Futuna,Mata-Utu"
  , "Yemen,Sanaa"
  , "Zambia,Lusaka"
  , "Zimbabwe,Harare"
  ]
