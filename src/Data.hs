{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE OverloadedStrings #-}


module Data
  ( closeDB
  , createContentCreatorProgramRelationship
  , createContentList
  , createContentListUnitRelationship
  , createContentUnitKeywordRelationship
  , createContributorContentUnitRelationship
  , createProgramContentUnitRelationship
  , createUser
  , createUserLike
  , deleteUserById
  , getContentUnitsByKeyword
  , getProgramContentById
  , getProgramContributors
  , getProgramNumLikes
  , getUserById
  , getUserContentLists
  , printQuery
  , runDB
  , showToText
  ) where


import           Control.Monad.Catch            ( MonadCatch
                                                , MonadThrow
                                                , throwM
                                                )
import           Control.Monad.IO.Class         ( MonadIO
                                                , liftIO
                                                )
import           Control.Monad.Reader           ( MonadReader
                                                , asks
                                                )
import           Data.Fixed                     ( Pico )
import qualified Data.Map                      as M
import           Data.Map                       ( Map )
import           Data.Maybe                     ( fromMaybe )
import           Data.Text                      ( Text )
import qualified Data.Text                     as T
import           Data.Time.Calendar             ( Day )
import           Data.Time.Clock                ( secondsToNominalDiffTime )
import           Data.Time.Clock.POSIX          ( POSIXTime )
import           Data.Time.Clock.POSIX          ( getPOSIXTime )
import           Data.UUID                      ( UUID
                                                , fromText
                                                , toText
                                                )
import           Data.UUID.V4                   ( nextRandom )
import           Database.Bolt                  ( (=:)
                                                , BoltActionT
                                                , Node(..)
                                                , Pipe
                                                , Value(B, I, N, T)
                                                , at
                                                , close
                                                , maybeAt
                                                , nodeProps
                                                , props
                                                , queryP
                                                , queryP_
                                                , run
                                                )

import           API.Types                      ( APIError(InternalError) )
import           Env                            ( Env(dbConn, logQueries) )
import           Types                          ( CCProgRelationShip
                                                , ContentList
                                                , ContentListLabel(UnitList)
                                                , ContentUnit
                                                , ContentUnitLabel(Unit)
                                                , ContentUnitPostRequest
                                                , KeywordLabel(Keyword)
                                                , LikeLabel(Like)
                                                , MembershipRelationShip
                                                  ( BELONGS_TO
                                                  , HAS
                                                  )
                                                , PaymentMethod(NoMethod)
                                                , ProgramLabel(ContentProgram)
                                                , ProgramPostRequest
                                                , User
                                                , UserLabel
                                                  ( AppUser
                                                  , FtArtist
                                                  , Musician
                                                  , Participant
                                                  , Podcaster
                                                  )
                                                , UserPostReq
                                                )
import qualified Types                         as CL
                                                ( ContentList(..) )
import qualified Types                         as CU
                                                ( ContentUnit(..) )
import qualified Types                         as CPR
                                                ( ContentUnitPostRequest(..) )
import qualified Types                         as UPR
                                                ( UserPostReq(..) )
import qualified Types                         as PPR
                                                ( ProgramPostRequest(..) )
import qualified Types                         as U
                                                ( User(..) )


createUser
  :: (MonadReader Env m, MonadIO m, MonadThrow m, MonadCatch m)
  => UserLabel
  -> UserPostReq
  -> m UUID
createUser uLabel req = do
  pipe      <- asks dbConn
  uuid      <- liftIO nextRandom
  timestamp <- liftIO getTimestamp
  let (userQr, ps) = mkUserQr "" uuid timestamp uLabel req
  printQueryEnv userQr
  runDB pipe $ queryP_ userQr ps
  return uuid

getUserById
  :: (MonadReader Env m, MonadIO m, MonadThrow m, MonadCatch m)
  => UserLabel
  -> UUID
  -> m (Maybe (User a))
getUserById label userId = do
  pipe <- asks dbConn
  printQueryEnv qr
  runDB pipe action
 where
  qr     = "MATCH (u:" <> showToText label <> ") WHERE u.idx = {idx} RETURN u"
  --
  action = do
    result  <- queryP qr (props ["idx" =: toText userId])
    records <- mapM (\record -> record `at` "u") result
    nodes   <- mapM (getProps userProps) records
    case nodes of
      [r] -> return $ Just $ recordToUser r
      _   -> return Nothing

-- This query deletes a user with all their relationships.
deleteUserById
  :: (MonadReader Env m, MonadIO m, MonadThrow m, MonadCatch m)
  => UserLabel
  -> UUID
  -> m ()
deleteUserById label userId = do
  pipe <- asks dbConn
  let qr = "MATCH (u:" <> showToText label <> "{ idx: {idx} }) DETACH DELETE u"
      ps = props ["idx" =: toText userId]
  printQueryEnv qr
  runDB pipe $ queryP_ qr ps

-- This query relates a content creator with a program, i.e., a show or an album
createContentCreatorProgramRelationship
  :: (MonadReader Env m, MonadIO m, MonadThrow m, MonadCatch m)
  => UserLabel
  -> ProgramLabel
  -> CCProgRelationShip
  -> UUID
  -> ProgramPostRequest
  -> m UUID
createContentCreatorProgramRelationship uLabel pLabel rl userId req
  | uLabel == Musician || uLabel == Podcaster = do
    pipe         <- asks dbConn
    (progId, ps) <- progProps
    printQueryEnv qr
    runDB pipe $ queryP_ qr ps
    return progId
  | otherwise = throwM InternalError
 where
  progProps = do
    uuid      <- liftIO nextRandom
    timestamp <- liftIO getTimestamp
    let ps = props
          [ "idx" =: toText uuid
          , "nm" =: PPR.name req
          , "ds" =: PPR.description req
          , "il" =: PPR.coverImageLink req
          , "rl" =: showToText (PPR.releaseDate req)
          , "ca" =: timestamp
          , "ua" =: timestamp
          ]
    return (uuid, ps)
  --
  qr :: Text
  qr = T.unwords
    [ "MATCH (u:" <> showToText uLabel <> ")"
    , "WHERE u.idx = '" <> toText userId <> "'"
    , "SET u.updated_at={ua}"
    , "CREATE (p:"
    <> concatLabels [ContentProgram, pLabel]
    <> " { idx: {idx}, name: {nm}, description: {ds}, created_at: {ca}, updated_at: {ua}, cover_image_link: {il}, release_date: {rl} })"
    , "WITH u, p"
    , "MERGE (u) -[:" <> showToText rl <> "]-> (p)"
    ]

getContentUnitsByKeyword
  :: (MonadReader Env m, MonadIO m, MonadThrow m, MonadCatch m)
  => ContentUnitLabel
  -> Text
  -> Int
  -> Int
  -> m [ContentUnit a]
getContentUnitsByKeyword cLabel word skip limit = do
  pipe <- asks dbConn
  printQueryEnv qr
  runDB pipe action
 where
  action = do
    result  <- queryP qr ps
    records <- mapM (\record -> record `at` "c") result
    nodes   <- mapM (getProps contentUnitProps) records
    return $ recordToContentUnit <$> nodes
  --
  ps = props ["word" =: word]
  --
  qr = T.unwords
    [ "MATCH (c:"
    <> showToText cLabel
    <> ")-[:"
    <> showToText HAS
    <> "]->(k:"
    <> showToText Keyword
    <> ")"
    , "WHERE k.keyword CONTAINS {word}"
    , "RETURN c"
    , "SKIP " <> showToText skip
    , "LIMIT " <> showToText limit
    ]

-- This query relates a contributor to a content unit
createContributorContentUnitRelationship
  :: (MonadReader Env m, MonadIO m, MonadThrow m, MonadCatch m)
  => UserLabel
  -> ContentUnitLabel
  -> CCProgRelationShip
  -> UUID
  -> UserPostReq
  -> m ()
createContributorContentUnitRelationship uLabel cLabel rl contentId req
  | uLabel == Participant || uLabel == FtArtist = do
    pipe     <- asks dbConn
    (qr, ps) <- mkQuery
    printQueryEnv qr
    runDB pipe $ queryP_ qr ps
  | otherwise = throwM InternalError
 where
  mkQuery = do
    timestamp <- liftIO getTimestamp
    uuid      <- liftIO nextRandom
    let (userQr, ps) = mkUserQr "u" uuid timestamp uLabel req
        queryProps   = M.insert "ua" (I timestamp) ps
        qr           = T.unwords
          [ "MATCH (c:" <> showToText cLabel <> ")"
          , "WHERE c.idx = '" <> toText contentId <> "'"
          , "SET c.updated_at={ua}"
          , userQr
          , "WITH u, c"
          , "MERGE (u) -[:" <> showToText rl <> "]-> (c)"
          ]
    return (qr, queryProps)

-- This query relates a program with a content unit, i.e.,
-- a song or an episode.
createProgramContentUnitRelationship
  :: (MonadReader Env m, MonadIO m, MonadThrow m, MonadCatch m)
  => ProgramLabel
  -> ContentUnitLabel
  -> UUID
  -> ContentUnitPostRequest
  -> m UUID
createProgramContentUnitRelationship pLabel cLabel progId req = do
  pipe            <- asks dbConn
  (ps, contentId) <- progProps
  printQueryEnv qr
  runDB pipe $ queryP_ qr ps
  return contentId
 where
  progProps = do
    uuid      <- liftIO nextRandom
    timestamp <- liftIO getTimestamp
    let duration :: Int
        duration = fromIntegral $ CPR.duration req
        ps =
          props
            $ [ "idx" =: toText uuid
              , "tl" =: CPR.title req
              , "if" =: CPR.isFree req
              , "icf" =: CPR.isChildFriendly req
              , "dr" =: duration
              , "ca" =: timestamp
              , "ua" =: timestamp
              , "ro" =:? (CPR.rightsOwner req)
              , "ds" =:? (CPR.description req)
              ]
    return (ps, uuid)
  --
  qr :: Text
  qr = T.unwords
    [ "MATCH (p:" <> showToText pLabel <> ")"
    , "WHERE p.idx = '" <> toText progId <> "'"
    , "SET p.updated_at={ua}"
    , "CREATE (c:"
    <> concatLabels [Unit, cLabel]
    <> " { idx: {idx}, title: {tl}, is_free: {if}, is_child_friendly: {icf}, duration: {dr},"
    <> "created_at: {ca}, rights_owner:{ro}, description:{ds}, updated_at: {ua} })"
    , "WITH p, c"
    , "MERGE (p) -[:" <> showToText HAS <> "]-> (c)"
    ]

-- This query relates a content unit with a keyword unit.
createContentUnitKeywordRelationship
  :: (MonadReader Env m, MonadIO m, MonadThrow m, MonadCatch m)
  => UUID
  -> Text
  -> m ()
createContentUnitKeywordRelationship contentId keyword = do
  pipe <- asks dbConn
  ps   <- progProps
  printQueryEnv qr
  runDB pipe $ queryP_ qr ps
 where
  progProps = do
    uuid      <- liftIO nextRandom
    timestamp <- liftIO getTimestamp
    return
      $ props
      $ [ "idx" =: toText uuid
        , "kw" =: keyword
        , "ca" =: timestamp
        , "ua" =: timestamp
        ]
  --
  qr :: Text
  qr = T.unwords
    [ "MATCH (c:" <> showToText Unit <> ")"
    , "WHERE c.idx = '" <> toText contentId <> "'"
    , "SET c.updated_at={ua}"
    , "CREATE (k:"
    <> showToText Keyword
    <> " { idx: {idx}, keyword: {kw}, created_at: {ca}, updated_at: {ua} })"
    , "WITH c, k"
    , "MERGE (c) -[:" <> showToText HAS <> "]-> (k)"
    ]

getProgramContentById
  :: (MonadReader Env m, MonadIO m, MonadThrow m, MonadCatch m)
  => ProgramLabel
  -> ContentUnitLabel
  -> MembershipRelationShip
  -> UUID
  -> m [ContentUnit a]
getProgramContentById pLabel cLabel rl progId = do
  pipe <- asks dbConn
  printQueryEnv qr
  runDB pipe action
 where
  qr =
    "MATCH (p:"
      <> showToText pLabel
      <> ")-[r:"
      <> showToText rl
      <> "]->(c:"
      <> showToText cLabel
      <> ") WHERE p.idx = {idx} RETURN c"
  --
  ps     = props ["idx" =: toText progId]
  --
  action = do
    result  <- queryP qr ps
    records <- mapM (\record -> record `at` "c") result
    nodes   <- mapM (getProps contentUnitProps) records
    return $ recordToContentUnit <$> nodes

createContentList
  :: (MonadReader Env m, MonadIO m, MonadThrow m, MonadCatch m)
  => UUID
  -> ContentListLabel
  -> Text
  -> m UUID
createContentList userId lLabel lName = do
  pipe         <- asks dbConn
  (ps, listId) <- listProps
  printQueryEnv qr
  runDB pipe $ queryP_ qr ps
  return listId
 where
  listProps = do
    uuid      <- liftIO nextRandom
    timestamp <- liftIO getTimestamp
    let ps =
          props
            $ [ "idx" =: toText uuid
              , "ln" =: lName
              , "ca" =: timestamp
              , "ua" =: timestamp
              ]
    return (ps, uuid)
  --
  qr :: Text
  qr = T.unwords
    [ "MATCH (u:" <> showToText AppUser <> ")"
    , "WHERE u.idx = '" <> toText userId <> "'"
    , "CREATE (l:"
    <> concatLabels [UnitList, lLabel]
    <> " { idx: {idx}, name: {ln}, created_at: {ca}, updated_at: {ua} })"
    , "WITH u, l"
    , "MERGE (u) -[:" <> showToText HAS <> "]-> (l)"
    ]

-- This query gets all the content lists created by users together with
-- their content units.
getUserContentLists
  :: (MonadReader Env m, MonadIO m, MonadThrow m, MonadCatch m)
  => ContentListLabel
  -> ContentUnitLabel
  -> UUID
  -> m [ContentList a b]
getUserContentLists lLabel cLabel userId = do
  pipe <- asks dbConn
  printQueryEnv qr
  runDB pipe action
 where
  recordToContentList :: ([Value], [[Value]]) -> ContentList a b
  recordToContentList (listR, contentsR) = CL.ContentList
    { CL.idx          = (uuidfromText . valueToText) $ listR !! 0
    , CL.name         = valueToText $ listR !! 1
    , CL.createdAt    = valueToPosixTime $ listR !! 2
    , CL.updatedAt    = valueToPosixTime $ listR !! 3
    , CL.contentUnits = recordToContentUnit <$> contentsR
    }
  action = do
    result <- queryP qr (props ["idx" =: toText userId])
    let tupleRecord attr1 attr2 record = do
          listRecord     <- record `at` attr1
          listNode       <- getProps contentListProps listRecord
          contentRecords <- record `at` attr2
          contentNodes   <- mapM (getProps contentUnitProps) contentRecords
          return (listNode, contentNodes)
    nodes <- mapM (tupleRecord "l" "cs") result
    let contentLists = recordToContentList <$> nodes
    return contentLists
  --
  qr = T.unwords
    [ "MATCH (u:"
    <> showToText AppUser
    <> ")-[r:"
    <> showToText HAS
    <> "]->(l:"
    <> showToText lLabel
    <> ")"
    , "WHERE u.idx = '" <> toText userId <> "'"
    , "OPTIONAL MATCH (c:"
    <> showToText cLabel
    <> ")-["
    <> showToText BELONGS_TO
    <> "]->(l)"
    , "RETURN l, collect(c) as cs"
    ]

-- This query gets all the contributors belonging to a specific album
getProgramContributors
  :: (MonadReader Env m, MonadIO m, MonadThrow m, MonadCatch m)
  => ProgramLabel
  -> ContentUnitLabel
  -> UserLabel
  -> CCProgRelationShip
  -> UUID
  -> m [User a]
getProgramContributors pLabel cLabel uLabel rl programId = do
  pipe <- asks dbConn
  printQueryEnv qr
  runDB pipe action
 where
  action = do
    result  <- queryP qr (props ["idx" =: toText programId])
    records <- mapM (\record -> record `at` "u") result
    nodes   <- mapM (getProps userProps) records
    return $ recordToUser <$> nodes
  --
  qr = T.unwords
    [ "MATCH (p:"
    <> showToText pLabel
    <> ")-[:"
    <> showToText HAS
    <> "]->(c:"
    <> showToText cLabel
    <> ")"
    , "WHERE p.idx = {idx}"
    , "MATCH (u:" <> showToText uLabel <> ")-[:" <> showToText rl <> "]->(c)"
    , "RETURN u"
    ]

createContentListUnitRelationship
  :: (MonadReader Env m, MonadIO m, MonadThrow m, MonadCatch m)
  => ContentListLabel
  -> ContentUnitLabel
  -> UUID
  -> UUID
  -> m ()
createContentListUnitRelationship lLabel cLabel listId contentId = do
  pipe <- asks dbConn
  ps   <- updatedAt
  printQueryEnv qr
  runDB pipe $ queryP_ qr ps
 where
  updatedAt = do
    timestamp <- liftIO getTimestamp
    return $ props $ ["ua" =: timestamp]
  --
  qr :: Text
  qr = T.unwords
    [ "MATCH (l:" <> showToText lLabel <> ")"
    , "WHERE l.idx = '" <> toText listId <> "'"
    , "MATCH (c:" <> showToText cLabel <> ")"
    , "WHERE c.idx = '" <> toText contentId <> "'"
    , "SET l.updated_at={ua}"
    , "WITH c, l"
    , "MERGE (l) -[:" <> showToText HAS <> "]-> (c)"
    , "MERGE (c) -[:" <> showToText BELONGS_TO <> "]-> (l)"
    ]

createUserLike
  :: (MonadReader Env m, MonadIO m, MonadThrow m, MonadCatch m)
  => ContentUnitLabel
  -> LikeLabel
  -> UUID
  -> UUID
  -> m ()
createUserLike cLabel lLabel userId contentId = do
  pipe <- asks dbConn
  ps   <- updatedAt
  printQueryEnv qr
  runDB pipe $ queryP_ qr ps
 where
  updatedAt = do
    uuid      <- liftIO nextRandom
    timestamp <- liftIO getTimestamp
    return
      $ props
      $ [ "idx" =: toText uuid
        , "ca" =: timestamp
        , "ua" =: timestamp
        , "uidx" =: toText userId
        ]
  --
  qr :: Text
  qr = T.unwords
    [ "MATCH (u:" <> showToText AppUser <> ")"
    , "WHERE u.idx = '" <> toText userId <> "'"
    , "MATCH (c:" <> showToText cLabel <> ")"
    , "WHERE c.idx = '" <> toText contentId <> "'"
    , "CREATE (l:"
    <> concatLabels [Like, lLabel]
    <> " { idx: {idx}, user_idx: {uidx}, created_at: {ca}, updated_at: {ua} })"
    , "WITH u, c, l"
    , "MERGE (c) -[:" <> showToText HAS <> "]-> (l)"
    , "MERGE (l) -[:" <> showToText BELONGS_TO <> "]-> (u)"
    ]


getProgramNumLikes
  :: (MonadReader Env m, MonadIO m, MonadThrow m, MonadCatch m) => UUID -> m Int
getProgramNumLikes programId = do
  pipe <- asks dbConn
  printQueryEnv qr
  runDB pipe action
 where
  action = do
    result  <- queryP qr (props ["idx" =: toText programId])
    records <- mapM (\record -> record `at` "n") result
    return $ head $ (\(I v) -> v) <$> records
  --
  qr = T.unwords
    [ "MATCH (p:"
    <> showToText ContentProgram
    <> ")-[:"
    <> showToText HAS
    <> "]->(c:"
    <> showToText Unit
    <> ")"
    , "WHERE p.idx = {idx}"
    , "MATCH (c)-[:" <> showToText HAS <> "]->(l:" <> showToText Like <> ")"
    , "RETURN count(c) as n"
    ]

closeDB :: MonadIO m => Pipe -> m ()
closeDB pipe = close pipe


-- Helpers
runDB :: MonadIO m => Pipe -> BoltActionT IO a -> m a
runDB pipe qr = liftIO $ run pipe qr

printQuery :: MonadIO m => Text -> m ()
printQuery qr = liftIO $ print qr >> putStrLn "--"

printQueryEnv :: (MonadReader Env m, MonadIO m) => Text -> m ()
printQueryEnv qr = do
  p <- asks logQueries
  if p then printQuery qr else return ()

getTimestamp :: IO Int
getTimestamp = (round . (* 1000)) <$> getPOSIXTime

getProps :: [Text] -> Node -> BoltActionT IO [Value]
getProps props' node = mapM (propAt node) props'
 where
  propAt :: Node -> Text -> BoltActionT IO Value
  propAt node' prop = do
    maybeV <- nodeProps node' `maybeAt` prop
    case maybeV of
      Just v  -> return v
      Nothing -> return $ N ()

concatLabels :: Show a => [a] -> Text
concatLabels = T.intercalate ":" . (fmap showToText)

showToText :: Show a => a -> Text
showToText = T.pack . show

valueToText :: Value -> Text
valueToText (T t) = t
valueToText _     = error "Invalid case"

maybeValueToText :: Value -> Maybe Text
maybeValueToText (N ()) = Nothing
maybeValueToText (T t ) = Just t
maybeValueToText _      = error "Invalid case"

maybeShowToProp :: Show a => Text -> Maybe a -> (Text, Value)
maybeShowToProp name Nothing  = (name, N ())
maybeShowToProp name (Just v) = (name, T $ showToText v)

(=:?) :: Show a => Text -> Maybe a -> (Text, Value)
(=:?) = maybeShowToProp

valueToPosixTime :: Value -> POSIXTime
valueToPosixTime (I n) =
  let secs :: Pico
      secs = fromIntegral n
  in  secondsToNominalDiffTime secs
valueToPosixTime _ = error "Invalid case"

valueToBool :: Value -> Bool
valueToBool (B b) = b
valueToBool _     = error "Invalid case"

valueToPaymentMethod :: Value -> PaymentMethod
valueToPaymentMethod (T t) = read $ T.unpack t
valueToPaymentMethod _     = error "Invalid case"

valueToDay :: Value -> Day
valueToDay (T t) = read $ T.unpack t
valueToDay _     = error "Invalid case"

valueToIntegral :: Integral a => Value -> a
valueToIntegral (I n) = fromIntegral n
valueToIntegral _     = error "Invalid case"

uuidfromText :: Text -> UUID
uuidfromText txt = fromMaybe (error "Corrupt uuid") (fromText txt)

recordToContentUnit :: [Value] -> ContentUnit a
recordToContentUnit r = CU.ContentUnit
  { CU.idx             = (uuidfromText . valueToText) $ r !! 0
  , CU.isFree          = valueToBool $ r !! 1
  , CU.isChildFriendly = valueToBool $ r !! 2
  , CU.title           = valueToText $ r !! 3
  , CU.duration        = valueToIntegral $ r !! 4
  , CU.rightsOwner     = maybeValueToText $ r !! 5
  , CU.description     = maybeValueToText $ r !! 6
  , CU.createdAt       = valueToPosixTime $ r !! 7
  , CU.updatedAt       = valueToPosixTime $ r !! 8
  }

recordToUser :: [Value] -> User a
recordToUser r = U.User { U.idx = (uuidfromText . valueToText) $ r !! 0
                        , U.firstName         = valueToText $ r !! 1
                        , U.lastName          = valueToText $ r !! 2
                        , U.email             = valueToText $ r !! 3
                        , U.nickname          = valueToText $ r !! 4
                        , U.preferredLanguage = valueToText $ r !! 5
                        , U.country           = valueToText $ r !! 6
                        , U.city              = valueToText $ r !! 7
                        , U.birthdate         = valueToDay $ r !! 8
                        , U.createdAt         = valueToPosixTime $ r !! 9
                        , U.updatedAt         = valueToPosixTime $ r !! 10
                        , U.isPremium         = valueToBool $ r !! 11
                        , U.paymentMethod     = valueToPaymentMethod $ r !! 12
                        }

contentUnitProps :: [Text]
contentUnitProps =
  [ "idx"
  , "is_free"
  , "is_child_friendly"
  , "title"
  , "duration"
  , "rights_owner"
  , "description"
  , "created_at"
  , "updated_at"
  ]

contentListProps :: [Text]
contentListProps = ["idx", "name", "created_at", "updated_at"]

userProps :: [Text]
userProps =
  [ "idx"
  , "first_name"
  , "last_name"
  , "email"
  , "nickname"
  , "preferred_language"
  , "country"
  , "city"
  , "birthdate"
  , "created_at"
  , "updated_at"
  , "is_premium"
  , "payment_method"
  ]

mkUserQr
  :: Text -> UUID -> Int -> UserLabel -> UserPostReq -> (Text, Map Text Value)
mkUserQr var uuid timestamp uLabel req = (qr, ps)
 where
  qr = T.unwords
    [ "CREATE (" <> var <> ":" <> concatLabels [AppUser, uLabel] <> " {"
    , " idx: {idx}"
    , ",created_at: {ca}"
    , ",updated_at: {ua}"
    , ",first_name: {fn}"
    , ",last_name: {ln}"
    , ",email: {em}"
    , ",nickname: {nm}"
    , ",preferred_language: {pl}"
    , ",country: {co}"
    , ",city: {ci}"
    , ",birthdate: {bd}"
    , ",is_premium: {ip}"
    , ",payment_method: {pm}"
    , "})"
    ]
  --
  ps = props
    [ "idx" =: toText uuid
    , "fn" =: UPR.firstName req
    , "ln" =: UPR.lastName req
    , "em" =: UPR.email req
    , "nm" =: UPR.nickname req
    , "pl" =: UPR.preferredLanguage req
    , "co" =: UPR.country req
    , "ci" =: UPR.city req
    , "bd" =: showToText (UPR.birthdate req)
    , "ca" =: timestamp
    , "ua" =: timestamp
    , "ip" =: False
    , "pm" =: showToText NoMethod
    ]
