{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings          #-}

module API.Handlers
  ( apiServer
  ) where

import           API                            ( API )
import           API.Types                      ( APIAction(unAPIAction)
                                                , actionE
                                                )
import           Control.Monad                  ( void
                                                , when
                                                )
import           Control.Monad.Trans.Reader     ( runReaderT )
import           Data.Maybe                     ( fromJust
                                                , fromMaybe
                                                , isNothing
                                                )
import           Data.Proxy                     ( Proxy(..) )
import           Data.Text                      ( Text )
import           Data.UUID                      ( UUID )
import           Servant                        ( (:<|>)(..)
                                                , Handler
                                                , Server
                                                , ServerT
                                                , hoistServer
                                                , throwError
                                                )

import           API.Types                      ( APIError
                                                  ( InternalError
                                                  , NonExsistentUser
                                                  )
                                                )
import           Data                           ( createContentCreatorProgramRelationship
                                                , createContentList
                                                , createContentListUnitRelationship
                                                , createContentUnitKeywordRelationship
                                                , createContributorContentUnitRelationship
                                                , createProgramContentUnitRelationship
                                                , createUser
                                                , createUserLike
                                                , deleteUserById
                                                , getContentUnitsByKeyword
                                                , getProgramContentById
                                                , getProgramContributors
                                                , getProgramNumLikes
                                                , getUserById
                                                , getUserContentLists
                                                )
import           Env                            ( Env )
import           Types                          ( CCProgRelationShip
                                                  ( FEATURES_IN
                                                  , HOSTS
                                                  , PARTICIPATES_IN
                                                  , RECORDED
                                                  )
                                                , ContentList
                                                , ContentListLabel
                                                  ( MusicList
                                                  , PodcastList
                                                  , UnitList
                                                  )
                                                , ContentUnit(..)
                                                , ContentUnitLabel
                                                  ( Episode
                                                  , Song
                                                  , Unit
                                                  )
                                                , ContentUnitPostRequest
                                                , LikeLabel
                                                  ( EpisodeLike
                                                  , SongLike
                                                  )
                                                , MembershipRelationShip(HAS)
                                                , NumLikes(..)
                                                , ProgramLabel
                                                  ( MusicAlbum
                                                  , PodcastShow
                                                  )
                                                , ProgramPostRequest
                                                , User
                                                , UserLabel
                                                  ( AppUser
                                                  , FtArtist
                                                  , Musician
                                                  , Participant
                                                  , Podcaster
                                                  )
                                                , UserPostReq
                                                )


statusH :: APIAction ()
statusH = return ()

postUserH :: UserLabel -> UserPostReq -> APIAction ()
postUserH label req = actionE $ void $ createUser label req

getUserH :: UserLabel -> UUID -> APIAction (User a)
getUserH label userId = actionE $ do
  maybeUser <- getUserById label userId
  when (isNothing maybeUser) (throwError $ NonExsistentUser label userId)
  return $ fromJust maybeUser

deleteUserH :: UserLabel -> UUID -> APIAction ()
deleteUserH label userId = actionE $ deleteUserById label userId

postContentCreatorAlbumH
  :: UserLabel -> UUID -> ProgramPostRequest -> APIAction ()
postContentCreatorAlbumH uLabel userId req
  | uLabel == Musician || uLabel == Podcaster = actionE $ do
    let (rl, pLabel) = determineRl uLabel
    void $ createContentCreatorProgramRelationship uLabel pLabel rl userId req
  | otherwise = actionE $ throwError InternalError
 where
  --
  determineRl :: UserLabel -> (CCProgRelationShip, ProgramLabel)
  determineRl Musician  = (RECORDED, MusicAlbum)
  determineRl Podcaster = (HOSTS, PodcastShow)
  determineRl _         = error "Invalid case"

postProgramContentH
  :: ProgramLabel
  -> ContentUnitLabel
  -> UUID
  -> ContentUnitPostRequest
  -> APIAction ()
postProgramContentH pLabel cLabel progId req =
  actionE $ void $ createProgramContentUnitRelationship pLabel cLabel progId req

getProgramContentsH
  :: ProgramLabel -> ContentUnitLabel -> UUID -> APIAction [ContentUnit a]
getProgramContentsH pLabel cLabel progId =
  actionE $ getProgramContentById pLabel cLabel HAS progId

postUserContentListH :: ContentListLabel -> UUID -> Text -> APIAction ()
postUserContentListH lLabel userId lName =
  actionE $ void $ createContentList userId lLabel lName

putListContentH
  :: ContentListLabel -> ContentUnitLabel -> UUID -> UUID -> APIAction ()
putListContentH lL cL listId contentId =
  actionE $ createContentListUnitRelationship lL cL listId contentId

getUserContentListsH :: ContentListLabel -> UUID -> APIAction [ContentList a b]
getUserContentListsH lLabel userId = actionE
  $ getUserContentLists lLabel cLabel userId
 where
  cLabel = case lLabel of
    MusicList   -> Song
    PodcastList -> Episode
    UnitList    -> Unit

postContentContributorH
  :: UserLabel -> ContentUnitLabel -> UUID -> UserPostReq -> APIAction ()
postContentContributorH uLabel cLabel contentId req =
  actionE
    $ createContributorContentUnitRelationship uLabel cLabel rl contentId req
 where
  rl = case cLabel of
    Song    -> FEATURES_IN
    Episode -> PARTICIPATES_IN
    _       -> error "Impossible case"


getProgramContributorsH :: ProgramLabel -> UUID -> APIAction [User a]
getProgramContributorsH pLabel programId = actionE
  $ getProgramContributors pLabel cLabel uLabel rl programId
 where
  (cLabel, uLabel, rl) = case pLabel of
    MusicAlbum  -> (Song, FtArtist, FEATURES_IN)
    PodcastShow -> (Episode, Participant, PARTICIPATES_IN)
    _           -> error "Impossible case"

postUserLikeH :: LikeLabel -> UUID -> UUID -> APIAction ()
postUserLikeH lLabel userId contentId = actionE
  $ createUserLike cLabel lLabel userId contentId
 where
  cLabel = case lLabel of
    EpisodeLike -> Episode
    SongLike    -> Song
    _           -> error "Impossible case"

getProgramNumLikesH :: UUID -> APIAction NumLikes
getProgramNumLikesH programId =
  actionE $ fmap (NumLikes . fromIntegral) (getProgramNumLikes programId)

putContentUnitKeywordH :: UUID -> Text -> APIAction ()
putContentUnitKeywordH contentId keyword =
  actionE $ createContentUnitKeywordRelationship contentId keyword

getContentUnitsByKeywordH
  :: ContentUnitLabel
  -> Text
  -> Maybe Int
  -> Maybe Int
  -> APIAction [ContentUnit a]
getContentUnitsByKeywordH cLabel word skip limit =
  actionE $ getContentUnitsByKeyword cLabel
                                     word
                                     (fromMaybe 0 skip)
                                     (fromMaybe 30 limit)


-- Server
handlers :: ServerT API APIAction
handlers =
  statusH
    :<|> postUserH AppUser
    :<|> getUserH AppUser
    :<|> postUserH Musician
    :<|> getUserH Musician
    :<|> postContentCreatorAlbumH Musician
    :<|> postContentContributorH FtArtist Song
    :<|> postUserH Podcaster
    :<|> getUserH Podcaster
    :<|> postContentCreatorAlbumH Podcaster
    :<|> postContentContributorH Participant Episode
    :<|> deleteUserH AppUser
    :<|> postUserContentListH MusicList
    :<|> postUserContentListH PodcastList
    :<|> getUserContentListsH MusicList
    :<|> getUserContentListsH PodcastList
    :<|> getUserContentListsH UnitList
    :<|> postUserLikeH SongLike
    :<|> postUserLikeH EpisodeLike
    :<|> postProgramContentH MusicAlbum Song
    :<|> getProgramContentsH MusicAlbum Song
    :<|> getProgramContributorsH MusicAlbum
    :<|> postProgramContentH PodcastShow Episode
    :<|> getProgramContentsH PodcastShow Episode
    :<|> getProgramContributorsH PodcastShow
    :<|> getProgramNumLikesH
    :<|> putContentUnitKeywordH
    :<|> getContentUnitsByKeywordH Song
    :<|> getContentUnitsByKeywordH Episode
    :<|> putListContentH MusicList   Song
    :<|> putListContentH PodcastList Episode

apiServer :: Env -> Server API
apiServer env = hoistServer (Proxy :: Proxy API) natT handlers
 where
  natT :: APIAction a -> Handler a
  natT m = runReaderT (unAPIAction m) env
