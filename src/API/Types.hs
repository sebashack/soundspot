{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}

module API.Types where

import           Control.Exception.Base         ( Exception )
import           Control.Monad.Catch            ( MonadCatch
                                                , MonadThrow
                                                )
import           Control.Monad.Except           ( ExceptT
                                                , MonadError(..)
                                                , runExceptT
                                                )
import           Control.Monad.IO.Class         ( MonadIO )
import           Control.Monad.Reader           ( MonadReader )
import           Control.Monad.Trans.Reader     ( ReaderT )
import           Data.Aeson                     ( ToJSON(..)
                                                , encode
                                                )
import qualified Data.Text                     as T
import           Data.Text                      ( Text )
import           Data.UUID                      ( UUID
                                                , toText
                                                )
import           GHC.Generics
import           Servant                        ( Handler
                                                , ServerError(..)
                                                )
import qualified Servant                       as SV
                                                ( throwError )
import           Servant.Server                 ( err404
                                                , err500
                                                )


import           Env                            ( Env )
import           Types                          ( UserLabel )


newtype APIAction a = APIAction
  { unAPIAction :: ReaderT Env Handler a
  } deriving ( Functor
             , Applicative
             , Monad
             , MonadIO
             , MonadReader Env
             , MonadThrow
             , MonadCatch
             )

instance MonadError ServerError APIAction where
  throwError e = APIAction $ SV.throwError e
  catchError m f = APIAction $ catchError (unAPIAction m) (unAPIAction . f)

type APIActionE e = ExceptT e APIAction

-- TODO: Fill up with errors as needed.
data APIError
     = NonExsistentUser UserLabel UUID
     | InternalError
     | OtherError Text
    deriving (Show)

instance Exception APIError

data PackedError = PackedError
  { errorDescription :: Text
  }
  deriving (Show, Generic)

instance ToJSON PackedError

class Exception e => ToPackedError e where
  toPackedErr :: e -> (PackedError, ServerError)

instance ToPackedError APIError where
  toPackedErr (NonExsistentUser label uuid) =
    let err = PackedError
          { errorDescription = (T.pack . show $ label)
                               <> " with id `"
                               <> toText uuid
                               <> "` not found"
          }
    in  (err, err404)
  toPackedErr InternalError =
    let err = PackedError { errorDescription = "Internal server error" }
    in  (err, err500)
  toPackedErr (OtherError desc) =
    let err = PackedError { errorDescription = desc } in (err, err500)

-- Enter the 'APIActionE' monad if you want to short-cut on API errors
-- so that you don't have to explicitly handle `Either` results.
actionE :: APIActionE APIError a -> APIAction a
actionE m = runExceptT m >>= either (failWith . toPackedErr) return
 where
  failWith :: (PackedError, ServerError) -> APIAction a
  failWith (err, errCode) = SV.throwError errCode { errBody = encode err }
