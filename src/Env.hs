{-# LANGUAGE DeriveGeneric     #-}

module Env where

import           Data.Aeson                     ( FromJSON )
import           Data.Default                   ( def )
import           Data.Text                      ( Text )
import           Database.Bolt                  ( BoltCfg
                                                  ( host
                                                  , password
                                                  , port
                                                  , user
                                                  )
                                                , Pipe
                                                , connect
                                                )
import           GHC.Generics


data Opts = Opts
  { appPort      :: Int
  , dbHost       :: String
  , dbPort       :: Int
  , dbUser       :: Text
  , dbPassword   :: Text
  , dbLogQueries :: Bool
  }
  deriving (Show, Generic)

instance FromJSON Opts

data Env = Env
  { dbConn     :: Pipe
  , logQueries :: Bool
  }

mkEnv :: Opts -> IO Env
mkEnv opts = do
  pipe <- connect $ def { user     = dbUser opts
                        , password = dbPassword opts
                        , port     = dbPort opts
                        , host     = dbHost opts
                        }
  return $ Env pipe (dbLogQueries opts)
