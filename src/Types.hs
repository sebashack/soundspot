{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE FlexibleInstances     #-}

module Types where

import           Data.Aeson                     ( FromJSON(..)
                                                , ToJSON
                                                )
import           Data.Text                      ( Text )
import           Data.Time.Calendar             ( Day )
import           Data.Time.Clock.POSIX          ( POSIXTime )
import           Data.UUID                      ( UUID )
import           GHC.Generics
import           Numeric.Natural                ( Natural )


-- User phantom labels
data Musician'
data Podcaster'
data Participant'
data FtArtist'
data AppUser'

-- Program phantom labels
data MusicAlbum'
data PodcastShow'

-- ContentUnit phantom labels
data Episode'
data Song'
data Unit'

-- ContentList phantom labels
data MusicList'
data PodcastList'
data UnitList'

data UserLabel = Musician | Podcaster | AppUser | Participant | FtArtist
  deriving (Show, Eq)

data ProgramLabel = MusicAlbum | PodcastShow | ContentProgram
  deriving (Show, Eq)

data ContentListLabel = MusicList | PodcastList | UnitList
  deriving (Show, Eq)

data ContentUnitLabel = Episode | Song | Unit
  deriving (Show, Eq)

data LikeLabel = SongLike | EpisodeLike | Like
  deriving (Show, Eq)

data KeywordLabel = Keyword
  deriving (Show, Eq)

data CCProgRelationShip = RECORDED | HOSTS | PARTICIPATES_IN | FEATURES_IN
  deriving (Show, Eq)

data MembershipRelationShip = HAS | BELONGS_TO
  deriving (Show, Eq)

data PaymentMethod = DebitCard | CreditCard | NoMethod
  deriving (Show, Eq, Generic, Read)

instance FromJSON PaymentMethod
instance ToJSON PaymentMethod

data UserPostReq = UserPostReq
  { firstName         :: Text
  , lastName          :: Text
  , email             :: Text
  , nickname          :: Text
  , preferredLanguage :: Text
  , country           :: Text
  , city              :: Text
  , birthdate         :: Day
  }
  deriving (Show, Generic)

instance FromJSON UserPostReq

data User a = User
  { idx               :: UUID
  , firstName         :: Text
  , lastName          :: Text
  , email             :: Text
  , nickname          :: Text
  , preferredLanguage :: Text
  , country           :: Text
  , city              :: Text
  , birthdate         :: Day
  , createdAt         :: POSIXTime
  , updatedAt         :: POSIXTime
  , paymentMethod     :: PaymentMethod
  , isPremium         :: Bool
  }
  deriving (Show, Generic)

instance ToJSON (User AppUser')
instance ToJSON (User Musician')
instance ToJSON (User Podcaster')
instance ToJSON (User Participant')
instance ToJSON (User FtArtist')

data ProgramPostRequest = ProgramPostRequest
  { name           :: Text
  , description    :: Text
  , coverImageLink :: Text
  , releaseDate    :: Day
  }
  deriving (Show, Generic)

instance FromJSON ProgramPostRequest

data Program a = Program
  { idx            :: UUID
  , name           :: Text
  , description    :: Text
  , releaseDate    :: Day
  , createdAt      :: POSIXTime
  , updatedAt      :: POSIXTime
  , coverImageLink :: Text
  }
  deriving (Show, Generic)

instance ToJSON (User MusicAlbum')
instance ToJSON (User PodcastShow')

data ContentUnitPostRequest = ContentUnitPostRequest
  { isFree          :: Bool
  , isChildFriendly :: Bool
  , title           :: Text
  , rightsOwner     :: Maybe Text
  , description     :: Maybe Text
  , duration        :: Natural
  }
  deriving (Show, Generic)

instance FromJSON ContentUnitPostRequest

data ContentUnit a = ContentUnit
  { idx             :: UUID
  , isFree          :: Bool
  , isChildFriendly :: Bool
  , title           :: Text
  , rightsOwner     :: Maybe Text
  , description     :: Maybe Text
  , duration        :: Natural
  , createdAt       :: POSIXTime
  , updatedAt       :: POSIXTime
  }
  deriving (Show, Generic)

instance ToJSON (ContentUnit Episode')
instance ToJSON (ContentUnit Song')
instance ToJSON (ContentUnit Unit')

data ContentList a b = ContentList
  { idx          :: UUID
  , name         :: Text
  , createdAt    :: POSIXTime
  , updatedAt    :: POSIXTime
  , contentUnits :: [ContentUnit b]
  }
  deriving (Show, Generic)

instance ToJSON (ContentList MusicList' Song')
instance ToJSON (ContentList PodcastList' Episode')
instance ToJSON (ContentList UnitList' Unit')

newtype NumLikes = NumLikes { numLikes :: Natural }
  deriving (Show, Generic)

instance ToJSON NumLikes
