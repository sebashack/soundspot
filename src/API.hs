{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE TypeOperators #-}

module API where

import           Data.Text                      ( Text )
import           Data.UUID                      ( UUID )
import           Servant                        ( (:<|>)
                                                , (:>)
                                                , Capture
                                                , Delete
                                                , Get
                                                , JSON
                                                , Post
                                                , Put
                                                , QueryParam
                                                , QueryParam'
                                                , ReqBody
                                                , Required
                                                )

import           Types                          ( AppUser'
                                                , ContentList
                                                , ContentUnit
                                                , ContentUnitPostRequest
                                                , Episode'
                                                , FtArtist'
                                                , MusicList'
                                                , Musician'
                                                , NumLikes
                                                , Participant'
                                                , PodcastList'
                                                , Podcaster'
                                                , ProgramPostRequest
                                                , Song'
                                                , Unit'
                                                , UnitList'
                                                , User
                                                , UserPostReq
                                                )


type RequiredQueryParam = QueryParam' '[Required]

type API
  =    "status" :> Get '[JSON] ()
  :<|> "user" :> ReqBody '[JSON] UserPostReq :> Post '[JSON] ()
  :<|> "user" :> Capture "user-id" UUID :> Get '[JSON] (User AppUser')
  :<|> "user" :> "musician" :> ReqBody '[JSON] UserPostReq :> Post '[JSON] ()
  :<|> "user" :> "musician" :> Capture "user-id" UUID :> Get '[JSON] (User Musician')
  :<|> "user" :> "musician" :> Capture "user-id" UUID :> "album"  :> ReqBody '[JSON] ProgramPostRequest :> Post '[JSON] ()
  :<|> "user" :> "ft-artist" :> "song" :> Capture "content-id" UUID :>  ReqBody '[JSON] UserPostReq :> Post '[JSON] ()
  :<|> "user" :> "podcaster" :> ReqBody '[JSON] UserPostReq :> Post '[JSON] ()
  :<|> "user" :> "podcaster" :> Capture "user-id" UUID :> Get '[JSON] (User Podcaster')
  :<|> "user" :> "podcaster" :> Capture "user-id" UUID :> "show" :> ReqBody '[JSON] ProgramPostRequest :> Post '[JSON] ()
  :<|> "user" :> "participant" :> "episode" :> Capture "content-id" UUID :>  ReqBody '[JSON] UserPostReq :> Post '[JSON] ()
  :<|> "user" :> Capture "user-id" UUID :> Delete '[JSON] ()
  :<|> "user" :> Capture "user-id" UUID :> "music-list" :> RequiredQueryParam "name" Text :> Post '[JSON] ()
  :<|> "user" :> Capture "user-id" UUID :> "podcast-list" :> RequiredQueryParam "name" Text :> Post '[JSON] ()
  :<|> "user" :> Capture "user-id" UUID :> "music-list" :> Get '[JSON] [ContentList MusicList' Song']
  :<|> "user" :> Capture "user-id" UUID :> "podcast-list" :> Get '[JSON] [ContentList PodcastList' Episode']
  :<|> "user" :> Capture "user-id" UUID :> "content-list" :> Get '[JSON] [ContentList UnitList' Unit']
  :<|> "user" :> Capture "user-id" UUID :> "song" :> Capture "content-id" UUID :> "like" :> Post '[JSON] ()
  :<|> "user" :> Capture "user-id" UUID :> "episode" :> Capture "content-id" UUID :> "like" :> Post '[JSON] ()
  :<|> "album" :> Capture "album-id" UUID :> "song" :> ReqBody '[JSON] ContentUnitPostRequest :> Post '[JSON] ()
  :<|> "album" :> Capture "album-id" UUID :> "song" :> Get '[JSON] [ContentUnit Song']
  :<|> "album" :> Capture "album-id" UUID :> "contributors" :> Get '[JSON] [User FtArtist']
  :<|> "show" :> Capture "show-id" UUID :> "episode" :> ReqBody '[JSON] ContentUnitPostRequest :> Post '[JSON] ()
  :<|> "show" :> Capture "show-id" UUID :> "episode" :> Get '[JSON] [ContentUnit Episode']
  :<|> "show" :> Capture "show-id" UUID :> "contributors" :> Get '[JSON] [User Participant']
  :<|> "program" :> Capture "program-id" UUID :> "likes" :> Get '[JSON] NumLikes
  :<|> "content" :> Capture "content-id" UUID :> "keyword" :> RequiredQueryParam "word" Text :> Put '[JSON] ()
  :<|> "song" :> RequiredQueryParam "word" Text :> QueryParam "skip" Int :> QueryParam "limit" Int :> Get '[JSON] [ContentUnit Song']
  :<|> "episode" :> RequiredQueryParam "word" Text :> QueryParam "skip" Int :> QueryParam "limit" Int :> Get '[JSON] [ContentUnit Episode']
  :<|> "music-list" :> Capture "list-id" UUID :> "song" :> Capture "content-id" UUID :> Put '[JSON] ()
  :<|> "podcast-list" :> Capture "list-id" UUID :> "episode" :> Capture "content-id" UUID :> Put '[JSON] ()
