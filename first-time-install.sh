#!/bin/bash

set -xeuf -o pipefail

export TOP_LEVEL="$( readlink -f "$( dirname "${BASH_SOURCE[0]}" )" )"

if [[ -v CI ]]; then
   SUDO=""
else
   SUDO="sudo"
fi

# Install system deps
${SUDO} apt-get update
${SUDO} apt-get install -y build-essential

# Install Haskell stack
if ! type "stack" > /dev/null; then
    curl -sSL https://get.haskellstack.org/ | sh
else
    echo 'stack already installed'
fi

# Install code formatter
stack install brittany-0.13.1.0

# Install Docker
if ! type "docker" > /dev/null; then
    ${SUDO} apt install -y apt-transport-https ca-certificates curl software-properties-common

    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | ${SUDO} apt-key add -

    ${SUDO} add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"

    ${SUDO} apt update

    ${SUDO} apt-cache policy docker-ce

    ${SUDO} apt install -y docker-ce
else
    echo 'docker already installed'
fi

#Install Docker Compose
if ! type "docker-compose" > /dev/null; then
    ${SUDO} curl -L https://github.com/docker/compose/releases/download/1.28.4/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
    ${SUDO} chmod +x /usr/local/bin/docker-compose
else
    echo 'docker-compose already installed'
fi
