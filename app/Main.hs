module Main where

import           Data.Yaml                      ( decodeFileEither )
import           Options.Applicative            ( Parser
                                                , ParserInfo
                                                , execParser
                                                , fullDesc
                                                , header
                                                , help
                                                , helper
                                                , info
                                                , long
                                                , metavar
                                                , progDesc
                                                , short
                                                , strOption
                                                )

import           API.Handlers                   ( apiServer )
import           Data.Proxy                     ( Proxy(..) )
import           Network.Wai.Handler.Warp       ( run )
import           Servant.Server                 ( serve )

import           API                            ( API )
import           Env                            ( Opts(..)
                                                , mkEnv
                                                )


newtype CmdOpts = CmdOpts
  { configPath :: String
  } deriving (Show)

main :: IO ()
main = do
  cmdOpts    <- execParser parserInfo
  eitherOpts <- decodeFileEither (configPath cmdOpts)
  case eitherOpts of
    Right opts -> do
      env <- mkEnv opts
      putStrLn $ "soundspot-server running on port: " ++ (show . appPort $ opts)
      let app = serve (Proxy :: Proxy API) (apiServer env)
      run (appPort opts) app
    Left e -> error $ show e

parserInfo :: ParserInfo CmdOpts
parserInfo = info
  (helper <*> optionParser)
  (fullDesc <> header "sounspot" <> progDesc "Soundspot server")

optionParser :: Parser CmdOpts
optionParser = CmdOpts <$> strOption
  (long "config-path" <> short 'c' <> metavar "CONFIGPATH" <> help
    "Absolute path to config-file"
  )
