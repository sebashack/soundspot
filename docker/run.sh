#!/bin/bash

set -xeuf -o pipefail

ROOT="$( readlink -f "$( dirname "${BASH_SOURCE[0]}" )" )"
DOCKER_FILE="${ROOT}/docker-compose.yaml"
DATA_DIR="${ROOT}/data"
LOGS_DIR="${ROOT}/logs"
NGINX_CONFIG="${ROOT}/nginx/nginx.conf"

cp -f  docker-compose-template.yaml docker-compose.yaml
sed -i "s|<path-to-data-dir>|${DATA_DIR}|g" docker-compose.yaml
sed -i "s|<path-to-logs-dir>|${LOGS_DIR}|g" docker-compose.yaml
sed -i "s|<path-to-nginx-config>|${NGINX_CONFIG}|g" docker-compose.yaml

docker-compose --file "$DOCKER_FILE" up --detach --remove-orphans
