module Main where

import           Data.Yaml                      ( decodeFileEither )
import           Env                            ( Env(dbConn)
                                                , mkEnv
                                                )
import           Options.Applicative            ( Parser
                                                , ParserInfo
                                                , execParser
                                                , fullDesc
                                                , header
                                                , help
                                                , helper
                                                , info
                                                , long
                                                , metavar
                                                , progDesc
                                                , short
                                                , strOption
                                                )

import           Data                           ( closeDB )
import           Schema                         ( createConstraints )

newtype CmdOpts = CmdOpts
  { configPath :: String
  } deriving (Show)


main :: IO ()
main = do
  cmdOpts    <- execParser parserInfo
  eitherOpts <- decodeFileEither (configPath cmdOpts)
  case eitherOpts of
    Right opts -> do
      pipe <- dbConn <$> mkEnv opts
      createConstraints pipe
      closeDB pipe
    Left e -> error $ show e

parserInfo :: ParserInfo CmdOpts
parserInfo = info
  (helper <*> optionParser)
  (fullDesc <> header "schema" <> progDesc "Soundspot schema")

optionParser :: Parser CmdOpts
optionParser = CmdOpts <$> strOption
  (long "config-path" <> short 'c' <> metavar "CONFIGPATH" <> help
    "Absolute path to config-file"
  )
