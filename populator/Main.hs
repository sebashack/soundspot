module Main where

import           Data.Time.Clock                ( diffUTCTime
                                                , getCurrentTime
                                                )
import           Data.Yaml                      ( decodeFileEither )
import           Env                            ( mkEnv )
import           Options.Applicative            ( Parser
                                                , ParserInfo
                                                , execParser
                                                , fullDesc
                                                , header
                                                , help
                                                , helper
                                                , info
                                                , long
                                                , metavar
                                                , progDesc
                                                , short
                                                , strOption
                                                )


import           Populator                      ( concatContentIds
                                                , createAlbumSongsSample
                                                , createAppUserSample
                                                , createEpisodeKeywordSamples
                                                , createEpisodeLikeSamples
                                                , createEpisodeParticipantsSamples
                                                , createListEpisodesSample
                                                , createListSongsSample
                                                , createMusicListSamples
                                                , createMusicianAlbumSamples
                                                , createMusicianSample
                                                , createPodcastListSamples
                                                , createPodcasterSample
                                                , createPodcasterShowSamples
                                                , createShowEpisodesSample
                                                , createSongFtArtistsSamples
                                                , createSongKeywordSamples
                                                , createSongLikeSamples
                                                , runPopulator
                                                )


newtype CmdOpts = CmdOpts
  { configPath :: String
  } deriving (Show)


main :: IO ()
main = do
  cmdOpts    <- execParser parserInfo
  eitherOpts <- decodeFileEither (configPath cmdOpts)
  startTime  <- getCurrentTime
  case eitherOpts of
    Right opts -> do
      env <- mkEnv opts
      putStrLn "Populating data-base ..."
      uuidsM  <- runPopulator env $ createMusicianSample 112
      uuidsP  <- runPopulator env $ createPodcasterSample 112
      uuidsU  <- runPopulator env $ createAppUserSample 118
      --
      uuidsML <- runPopulator env $ createMusicListSamples 10 uuidsU
      uuidsPL <- runPopulator env $ createPodcastListSamples 9 uuidsU
      --
      uuidsAL <- runPopulator env $ createMusicianAlbumSamples 12 uuidsM
      uuidsSH <- runPopulator env $ createPodcasterShowSamples 5 uuidsP
      --
      uuidsAS <- runPopulator env $ createAlbumSongsSample 10 uuidsAL
      uuidsSE <- runPopulator env $ createShowEpisodesSample 15 uuidsSH
      --
      let songUuids    = concatContentIds uuidsAS
          episodeUuids = concatContentIds uuidsSE
      --
      runPopulator env $ createSongKeywordSamples uuidsAS
      runPopulator env $ createEpisodeKeywordSamples uuidsSE
      --
      runPopulator env $ createSongFtArtistsSamples 3 uuidsAS
      runPopulator env $ createEpisodeParticipantsSamples 4 uuidsSE
      --
      runPopulator env $ createListSongsSample 8 uuidsML songUuids
      runPopulator env $ createListEpisodesSample 8 uuidsPL episodeUuids
      --
      runPopulator env $ createSongLikeSamples 16 uuidsU songUuids
      runPopulator env $ createEpisodeLikeSamples 15 uuidsU episodeUuids
      --
      endTime <- getCurrentTime
      print
        $  "Finished successfully in: "
        <> show (diffUTCTime endTime startTime)
        <> " secs"
    Left e -> error $ show e

parserInfo :: ParserInfo CmdOpts
parserInfo = info
  (helper <*> optionParser)
  (fullDesc <> header "populator" <> progDesc "Soundspot populator")

optionParser :: Parser CmdOpts
optionParser = CmdOpts <$> strOption
  (long "config-path" <> short 'c' <> metavar "CONFIGPATH" <> help
    "Absolute path to config-file"
  )
